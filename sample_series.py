#Import helper modules
import scintillation_module.dataProcessor.classHandler as ch
import os

#Location of the beamformed h5 dynamic spectra
observationLoc = "/data001/scratch/mckenna/IPS_proc/"

# Provide as the current setup has all of the CME observations in a given folder
fileContains = ["_3C196_", "_3C147_", "_3C159_", "_3C186_"]

for fileKey in fileContains:
	# Get a list of files to analyse
	fileList = [observationLoc + fileVar for fileVar in os.listdir(observationLoc) if (fileKey in fileVar)];
	
	#Create the spectral series object from the list
	chObj = ch.createSpectralSeries(fileList)

	#Iterate over our methods and save a variety of data (figures, raw data by default)
	for method in ['powerSpectrum', "variability"]:
		chObj.processingOptions['method'] = method
		chObj.determineIndex()

		chObj.visualise(freqIdx = 'all'); chObj.visualise();
		chObj.visualise(freqIdx = 'all', medianVals = True); chObj.visualise(medianVals = True)
		chObj.visualise(freqIdx = 'all', medianVals = True, errorBars = False); chObj.visualise(medianVals = True, errorBars = False)
