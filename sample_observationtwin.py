#Import some helper functions
import scintillation_module.dataProcessor.classHandler as ch
import scintillation_module.tools.defaultParams as dp

# Create a list of keys to pass to our file location generator
sourcesList = [['3C147', 'june22'], ['3C147', 'june28'], ['3C48', 'april'], ['3C48', 'march']]

#Iterating...
for target, date in sourcesList:
	#imgloc: Image cube h5 file location
	#imggroup: The name of the folder within the h5 for the observation
	#h5loc: Beamforme ddyanmic spectrum location
	imgloc, imggroup, h5loc = dp.returnVals(target, date)

	#With out generated values, create the object
	twinobj = ch.createAnalysisTwin(image_group_name=imggroup, source_image_cube=imgloc, source_dynamic_spectrum=h5loc)

	#Determine the indicies. By default, this samples by methods
	twinobj.determineIndex()

	#Save off figures and raw data for the output
	twinobj.visualise(freqIdx = 'all'); twinobj.visualise(medianVals = True, freqIdx = 'all')
	twinobj.visualise(); twinobj.visualise(medianVals = True)
