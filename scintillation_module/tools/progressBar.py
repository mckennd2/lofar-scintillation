"""Generate a progress bar for a given operation.

Attributes:
    estTime (deque): Self-length-limiting list that will estimate the time remaining for the operation.
    wipPixels (list): Characters to use as the 'working pixel'
"""
from __future__ import print_function
import datetime
import time
import subprocess
import numpy as np

from subprocess import PIPE
from collections import deque

def handle(proc, trigger, totalOp):
	"""Handler for progress bar, listens for progress updates
	
	Args:
	    proc (subprocess (?): Subprocess to list to the output of.
	    trigger (string): String to update count on
	    totalOp (int): Expected operations
	"""
	with proc.stdout:
		count = -1
		startTime = time.time()
		currentPixelIdx = 0
		for line in iter(proc.stdout.readline, ''):
			if trigger in line:
				count += 1
				progress = count / float(totalOp)
				width = int(subprocess.check_output(['stty', 'size']).split()[1])
				currentPixelIdx = handleBar(startTime, progress, width, currentPixelIdx)
	proc.wait()

def timeRemaining(progress, timeElapsed):
	"""Remaining time estimate, will update the estTime deque (length limited) List
	
	Args:
	    progress (float): Current progress (0-1)
	    timeElapsed (Time): Current progress
	
	Returns:
	    string: Time remaining (human-readable)
	"""
	if progress < 0.01:
		return "--:--:--"

	estRemain = int(timeElapsed / progress - timeElapsed)
	estTime.append(estRemain)

	estRemain = int(np.mean(estTime))

	timeString = str(datetime.timedelta(seconds=estRemain))

	return timeString

global wipPixels
wipPixels = ["-", "\\", "|", "/", "-", "\\", "|", "/"]


global estTime
estTime = deque(maxlen = 10)

def progressbar(length, progress, currentPixelIdx):
	"""Construct the progress bar
	
	Args:
	    length (int): Width of the bar
	    progress (float): Current progress (0-1)
	    currentPixelIdx (int): Current 'working' pixel index
	
	Returns:
	    List: Progress bar string
	    int: currentPixelIdx + 1
	"""
	length -= 5
	progressPixels = int(length * progress)
	remainingPixels = length - progressPixels

	progressPixels  *= "="
	remainingPixels *= "."
	if currentPixelIdx == len(wipPixels):
		currentPixelIdx = 0
	workingPixel = wipPixels[currentPixelIdx]

	construction = " [{0}{1}{2}] ".format(progressPixels, workingPixel, remainingPixels)

	return construction, currentPixelIdx + 1

def handleBar(startTime, progress, columns = None, currentPixelIdx = 0):
	"""Progress bar creator
	
	Args:
	    startTime (Time): Initial time to reference for remaining time
	    progress (float): Current progress (0-1)
	    columns (int): Width of the console in symbols
	    currentPixelIdx (int): Current 'working' pixel index
	
	Returns:
	    currentPixelIdx (int): currentPixelIdx + 1
	"""

	if not columns:
		columns = int(subprocess.check_output(['stty', 'size']).split()[1])


	timeRemainingVar = timeRemaining(progress, time.time() - startTime)
	progressStr = int(len("{:.2%}".format(progress)))
	progressBarVar, currentPixelIdx = progressbar(columns - len(timeRemainingVar) -progressStr, progress, currentPixelIdx)

	print("{0}{1}{2:.2%}".format(timeRemainingVar, progressBarVar, progress), end = "\r")

	return currentPixelIdx


def execute(args):
	"""Execute a shell command as a subprocess, pipe out output, pass on the reference
	
	Args:
	    args (string): Command arguments
	
	Returns:
	    subprocess: Subprocess reference
	"""
	proc = subprocess.Popen(args, stdout=PIPE, shell = True, bufsize = 1)
	return proc
