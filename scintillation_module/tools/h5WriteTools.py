"""Helper functions for reading / writing image data to and from FITS and h5 datasets

Attributes:
    dataStoreLarge (TYPE): Define the preferred data type for raw imaging observations
"""
import h5py
import numpy as np
from datetime import datetime
import os.path

import astropy.time as aptime
from astropy import wcs 
from astropy.io import fits
from astropy.coordinates import get_sun, SkyCoord



global dataStoreLarge

dataStoreLarge = np.float32 # Observations


def initialiseObservation(newFileName, referenceFits, commandHash, attrs):
	"""Setup a h5 file with everyhting needed to include a new observation
	
	Args:
	    newFileName (string): h5 file location of output data
	    referenceFits (string): .FITS file location to use as a basis to determine shape, metadata, etc.
	    commandHash (string): Hash of the head command
	    attrs (dict): Dictionary of values to add as head attributes
	
	Returns:
	    string: Observation group name
	"""
	h5FileLoc = newFileName

	__, hashVal = commandHash
	if not os.path.isfile(h5FileLoc):
		initGroups(h5FileLoc)

	with fits.open(referenceFits) as refFits:
		referenceShape = np.array(refFits[0].data.shape)
		timeStart = refFits[0].header["date-obs"]
		fitsHistory = str(refFits[0].header["history"])

	referenceShape[1] = attrs["dataCount"]
	groupName = "{0}-{1}".format(timeStart, hashVal)
	
	with h5py.File(h5FileLoc, 'r+') as fileRef:
		if not fileRef.__contains__("observations/" + groupName):
			initObservation(h5FileLoc, groupName, referenceShape, attrs, commandHash, fitsHistory)

	return "observations/" + groupName

def initGroups(fileLoc):
	"""Initialise a set of groups for a new target, intended for the first usage of a file (errors if file exists)
	
	Args:
	    fileLoc (string): Intended file location
	"""
	with h5py.File(fileLoc, 'w-') as refFile:
		refFile.require_group("observations")
		refFile.require_group("analysis")
		refFile.require_group("command_reference")

		refFile.attrs.create("version", 0.1)
		refFile.attrs.create("init_time", str(datetime.now()))

def initObservation(fileLoc, groupName, dataShape, attrs, commandHash, fitsHistory):
	"""Initialise the group for a new observation of a target
	
	Args:
	    fileLoc (string): Intended file location
	    groupName (string): Observation groupname
	    dataShape (tuple, array-like): Shape of the imaging data
	    attrs (dict): Dictionary of attribues to add to observation group.
	    commandHash (list): List containing the command and hash value for the observation.
	"""

	commandVal, hashVal = commandHash
	with h5py.File(fileLoc) as refFile:
		groupRef = refFile["observations"].require_group(groupName)
		for key, val in attrs.items():
			groupRef.attrs.create(key, val)

		groupRef.require_dataset("image_data", tuple(dataShape), dtype = dataStoreLarge, compression = "lzf")
		groupRef.require_group("raw_headers")
		groupRef.require_group("command_map")
		groupRef.require_group("location_ref")

		refFile["command_reference"].attrs.create(hashVal[:7], "".join(fitsHistory.split("\n")))
		refFile["command_reference"].attrs.create(hashVal[:7] + "-obsgroup", groupName)


def processChunk(fileLoc, fitsLocs, groupName, dataRange, wscRange):
	"""Process a data segment
	
	Args:
	    fileLoc (string): Intended file location
	    fitsLocs (List): Ordered lsit of .FITS file locations
	    groupName (string): Observation groupname
	    dataRange (List): Start / End of .FITS file in the h5 output in time steps
	    wscRange (List): Start / End of sampled interval in MS time steps
	"""
	with h5py.File(fileLoc) as refFile:
		startIdx = dataRange[0]
		groupRef = refFile[groupName]
		headerArray = []
		wcsArray = []
		with fits.open(fitsLocs[0]) as refFits:
			groupRef["command_map"].attrs.create("-".join(map(str, dataRange)), wscRange)
			writeShape = np.array(refFits[0].data.shape)
			writeShape[1] = len(fitsLocs)
			writeCache = np.zeros(writeShape)

			writeCache[:, 0, ...] = refFits[0].data[:, 0, ...]

			wcsOutput = handle_wcs_variables(refFits, [writeShape[-2], writeShape[-1]], True)

			wcsArray.append([startIdx, wcsOutput])
			headerArray.append([startIdx, refFits[0].header.items()])

		refIdx = 1
		for fitsLoc in fitsLocs[1:]:
			with fits.open(fitsLoc) as fitsRef:
				writeCache[:, refIdx, ...] = fitsRef[0].data[:, 0, ...]
				headerArray.append([startIdx + refIdx, refFits[0].header.items()])

				if refIdx % 50 == 0:
					wcsOutput = handle_wcs_variables(fitsRef, [writeShape[-2], writeShape[-1]], True)
					wcsArray.append([startIdx + refIdx, wcsOutput])

				refIdx += 1
		
		groupRef["image_data"][:, dataRange[0]:dataRange[1], ...] = writeCache

		for chunk in headerArray:
			headerGroupRef = groupRef["raw_headers"].require_group(str(chunk[0]))
			for key, val in chunk[1]:
				headerGroupRef.attrs.create(key, val)

		for chunk in wcsArray:
			headerGroupRef = groupRef["location_ref"].require_group(str(chunk[0]))
			headerGroupRef.attrs.create("solar_elongation", chunk[1][0])
			headerGroupRef.attrs.create("location", chunk[1][1])


def header_attrs_creator(fileRef, group, iterable):
	"""Apply a dictionary of keys and values to the attributes of a group
	
	Args:
	    fileLoc (string): Intended file location
	    group(string): Observation groupname
	    iterable (iter, dict): Iterable object (dict, etc.)
	"""
	if group in fileRef:
		groupRef = fileRef[group]
		for key, val in iterable.iteritems():
			groupRef.attrs.modify(key, val)
	else:
		groupRef = fileRef.require_group(group)
		for key, val in iterable.iteritems():
			groupRef.attrs.create(key, val)
	del groupRef



### Not sure this is actually correct. We need to get solar elongation along the plane, rather than at an arbitrary degree?
def handle_wcs_variables(fitsRef, imageShape, workaroundCDELT3 = True):
	"""Process the FITS headers and save off information about the central pixel and sun
	
	Args:
	    fitsRef (astropy.io.FITS): .FITS file open reference to read
	    imageShape (array-like): Shape of the image
	    workaroundCDELT3 (bool, optional): WSClean has a 3rd dimension (W) that astropy cannot correctly handle, correct for it by setting the step to 1 from 0?
	
	Returns:
	    elongationAU (float): Estimated Solar Elongation in AU
	    stringLoc (?): Location of the central pixel on the sky
	"""
	if workaroundCDELT3:
		fitsRef[0].header["CDELT3"] = 1.

	wVar = wcs.WCS(fitsRef[0].header)
	obsTime = aptime.Time(fitsRef[0].header["DATE-OBS"])
	
	# Linear approximation between the corners and center due to computation time required by this step.
	# Probably should find a better way to implement this.
	centralPixelLoc = SkyCoord.from_pixel(int(imageShape[0] / 2), int(imageShape[1] / 2), wVar)

	solarCoord = get_sun(obsTime)
	elongation = centralPixelLoc.position_angle(solarCoord)
	
	elongationAU = np.sin(elongation)
	print(elongation, elongationAU)

	stringLoc = str(centralPixelLoc.to_string())

	return elongationAU, stringLoc
