"""Class based around easy creation of data handler objects, only command to import / clean data sets.
"""
from ..dataType import dynamicSpectrum, imageCube, scintillatingObject

def createDynamicSpectrumObject(source_file, processing_options = None, autoClean = False):
	"""Creates a DS object from the given observation and performs RFI cleaning if provided a dictionary from ..tools.defaultDicts
	
	Args:
	    source_file (string): Dynspec v2 pipeline output h5 file location.
	    processing_options (dict, optional): RFI cleaning dictionary.
	    autoClean (bool, optional): Force cleaning even if parameters are not provided (uses defualts)
	
	Returns:
	    PreparedSpectrum: Dynamic Spectrum object ready for self.determineIndex()
	"""
	if processing_options or autoClean:
		dynamicSpectrumObj = dynamicSpectrum.RawSpectrum(source_file = source_file, cleaning_options = processing_options).clean()
	else:
		dynamicSpectrumObj = dynamicSpectrum.PreparedSpectrum(source_file = source_file)

	return dynamicSpectrumObj



def createImageCubeObject(source_file, group_name, processing_options = None, autoClean = True):
	"""Creates an IC object from a given image cube H5 produced by the script in .imaging/imaging_automation.py
	
	Args:
	    source_file (string): Image cube h5 file location
	    group_name (string): Observation group name, eg "observations/20XX-XX-XXTXX:XX:XX-hashval"
	    processing_options (dict, optional): Analysis options, defaults found in ..tools.defaultDicts
	    autoClean (bool, optional): Force analysis even if parameters are not provided (uses defaults)
	
	Returns:
	    PreapredImageCube: Image Cube object ready for self.determineIndex()
	"""
	if processing_options or autoClean:
		imageCubeObj = imageCube.RawImageCube(source_file = source_file, group_name = group_name, scintillating_options = processing_options).identifyPixels()
	else:
		imageCubeObj = imageCube.PreparedImageCube(source_file = source_file, group_name = group_name)

	return imageCubeObj



def createAnalysisTwin(source_dynamic_spectrum, source_image_cube, image_group_name, skip_ds_cleaning = True, ds_cleaning_opt = None, skip_image_analysis = False, image_identification_opt = None):
	"""Create an ObservationTwins object for easly analysis via default parameters; clean as requested.
	
	Args:
	    source_dynamic_spectrum (string): Dynamic Spectrum h5 file location
	    source_image_cube (string): Image Cube h5 file location
	    image_group_name (string): Image Cube group name, eg "observations/20XX-XX-XXTXX:XX:XX-hashval"
	    skip_ds_cleaning (bool, optional): Force DS cleaning even if no dictionary is provided (uses defaults)
	    ds_cleaning_opt (dict, optional): DS Cleaning dictionary
	    skip_image_analysis (bool, optional): Force IC pixel identification if no dictionary is provided (uses defaults)
	    image_identification_opt (dict, optional): IC pixel identification dictionary
	
	Returns:
	    ObservationTwins: An object containing references to both a DS and IC object, self.determineIndex() will process all methods and objects.
	"""
	processedSpectrum = createDynamicSpectrumObject(source_dynamic_spectrum, processing_options = ds_cleaning_opt, autoClean = not skip_ds_cleaning)
	processedImageCube = createImageCubeObject(source_image_cube, image_group_name, processing_options = image_identification_opt, autoClean = not skip_image_analysis)

	analysisTwin = scintillatingObject.ObservationTwins(processedSpectrum = processedSpectrum, processedImageCube = processedImageCube)

	return analysisTwin

def createSpectralSeries(source_dynamic_spectra, processing_options = None, perform_cleaning = False, cleaning_options = None):
	"""Create a SpectralSeries object for easy analysis of sequential observation windows.
	
	Args:
	    source_dynamic_spectra (TYPE): Input h5 files locations
	    processing_options (dict, optional): Scintillstion index processing dictionary
	    perform_cleaning (bool, optional): Whether or not to clean the dataset via rfiFlagger
	    cleaning_options (dict, optional): Cleaning dictionary
	
	Returns:
	    SepctralSeries: An object setup to manage several sequential observations, self.determineIndex() will process all observations and generate time axis values in self.xVals
	"""
	if perform_cleaning:
		source_dynamic_spectra_clean = []
		
		for sourceFile in source_dynamic_spectra:
			if cleaning_options:
				cleaning_options['inputFile'] = sourceFile
				cleaning_options['outputFile'] = "/".join(cleaning_options['outputFile'].split("/")[:-1]) + "/" + sourceFile[:-3] + "_rfi_flagged.h5"
			cleanRef = createDynamicSpectrumObject(sourceFile, processing_options = cleaning_options, autoClean = True)
			source_dynamic_spectra_clean.append(cleanRef.sourceFile)

		source_dynamic_spectra = source_dynamic_spectra_clean

	spectralSeries = dynamicSpectrum.SpectralSeries(source_dynamic_spectra, processing_options = processing_options)

	return spectralSeries
