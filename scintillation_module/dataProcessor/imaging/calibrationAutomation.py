"""CLI for automated calibration of inteprlantary scintillation imaging. UNTESTED.

Attributes:
    parser (TYPE): Description
"""
from argparse import ArgumentParser
from subprocess import PIPE

import os.path
import subprocess
import sys

parser = ArgumentParser("Process a MS observation through DPPP to perform calibration.")
parser.add_argument('-cepServer', help = "Are we on a CEP server and can load modules as a result?", default = True, dest = 'cepServer')
parser.add_argument('-i', help="Input MS Folder(s) (comma separated)", default = "./", dest = 'input_ms')
parser.add_argument('-iCol', help = "Input column in the observation to calibrate", default = "DATA", dest = 'input_col')
parser.add_argument('-iOut', help = "Output folder when we need to merge subbands", default = "./observation_raw.MS/", dest = 'input_output')
parser.add_argument('-avg', help = "Perform time sample averaging to speed up calibration", default = True, dest = 'avg_bool')
parser.add_argument('-avgSteps', help = "if performing averaging for calibration, how many steps should be taken", default = 6, dest = 'avg_steps')
parser.add_argument('-avgClean', help = "Determine whether or not to remove time data when it is no longer needed", default = True, dest = 'avg_clean')
parser.add_argument('-skymodel', help = "Name of the skymodel in the resources folder to use.", default = None, dest = 'skymodel')
parser.add_argument('-o', help = "Output Folder for calibrated results (defaults to input if left as None)", default = None, dest = 'output_ms')
parser.add_argument('-oCol', help = "Output column for calibrated data in output folder", default = "CORRECTED_DATA", dest = 'output_col')


def main(argv, getVals = False):
	"""Summary
	
	Args:
	    argv (TYPE): Description
	    getVals (bool, optional): Description
	
	Returns:
	    TYPE: Description
	"""
	args = parser.parse_args(argv)
	if getVals:
		return args.__dict__

	if not argv:
		print("You didnt pass any arguments? Here are my default values.")
		print(args.__dict__)
		inputVar = raw_input("Manually set arguments? y/[n]") or 'n'
		if inputVar.lower() == 'y':
			for key in args.__dict__:
				value = args.__dict__[key]
				args.__dict__[key] = type(value)(raw_input("{0}: {1}, {2}".format(key, value, type(value))) or value)
		else: 
			return None

	if args.cepServer:
		performCommand("module load lofim")

	if len(args.input_ms.split(',')) > 1:
		mergeList = ",".join(args.input_ms.split(',')) # Ensure we don't only pass a reference
		args.input_ms = args.input_output
	else:
		mergeList = False

	if not args.output_ms:
		args.output_ms = args.input_ms

	if args.output_ms[-1] == '/':
		indexVar = 2
	else:
		indexVar = 1

	workFolder = "/".join(args.output_ms.split('/')[:-1 * indexVar])

	if mergeList:
		readWriteExecute(workFolder, "/DPPP_merge.parset", [mergeList, args.input_col, args.input_output])
		args.input_col = "DATA"

	if args.avg_bool:
		avgInput = workFolder + "/observation_avg.MS/"
		avgCol = "DATA"
		readWriteExecute(workFolder, "/DPPP_avg.parset", [args.input_ms, args.input_col, avgInput, args.avg_steps])
	else:
		avgInput = args.input_ms
		avgCol = args.input_col
	
	skymodelLoc = os.path.join(os.path.dirname(__file__), 'resources', '{0}.parset'.format(args.skymodel))
	currentCommand = "makesourcedb in={0}.skymodel out={1}/sky format='<'".format(skymodelLoc, avgInput)
	performCommand(currentCommand)

	readWriteExecute(workFolder, "/DPPP_calibrate.parset", [avgInput, avgCol])
	readWriteExecute(workFolder, "/DPPP_apply_calibration.parset", [args.input_ms, args.input_col, args.output_ms, args.output_col, avgInput])


def readWriteExecute(workFolder, parsetName, parsetVars):
	"""Summary
	
	Args:
	    workFolder (TYPE): Description
	    parsetName (TYPE): Description
	    parsetVars (TYPE): Description
	"""
	outputParsetLoc = workFolder + parsetName
	with open(os.path.join(os.path.dirname(__file__), 'resources', parsetName[1:]), 'r') as fileRef:
		currParset = fileRef.readlines()
		currParset = "\n".join(currParset)
		currParset = currParset.format(parsetVars)
		currParset = currParset.split("\n")
		with open(outputParsetLoc, 'w') as outRef:
			outRef.writelines(currParset)

	currentCommand = 'DPPP {0}'.format(outputParsetLoc)
	performCommand(currentCommand)


def executeCommand(command):
	"""Summary
	
	Args:
	    command (TYPE): Description
	
	Returns:
	    TYPE: Description
	"""
	proc = subprocess.Popen(command, stdout=PIPE, shell = True, bufsize = 1)
	return proc

def handleCommand(proc):
	"""Summary
	
	Args:
	    proc (TYPE): Description
	"""
	with proc.stdout:
		for line in iter(proc.stdout.readline, ''):
			print(line)
	proc.wait()

def poller(process):
	"""Summary
	
	Args:
	    process (TYPE): Description
	
	Returns:
	    TYPE: Description
	"""
	while process.poll() is None:
		continue
	return process.poll()

def checkExit(exitCode, command):
	"""Summary
	
	Args:
	    exitCode (TYPE): Description
	    command (TYPE): Description
	
	Raises:
	    RuntimeError: Description
	"""
	if exitCode == 127:
		raise RuntimeError("A module wasn't loaded for command {0}".format(command))

	if exitCode == 255:
		raise RuntimeError("The program had an issue with the passed flags / data for command {0}".format(command))

def performCommand(currentCommand):
	"""Summary
	
	Args:
	    currentCommand (TYPE): Description
	"""
	process = executeCommand(currentCommand)

	handleCommand(process)
	exitCode = poller(process)
	checkExit(exitCode, currentCommand)

# If called via CLI, pass along args.
if __name__ == '__main__':
		main(sys.argv[1:])
