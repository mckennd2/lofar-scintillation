"""Simplified RFI routine: currently only normalising intensity between bands. Needs dire replacement of RFI flagger, bandpass normaliser is functional.

Possible upgrade for bandpass normaliser: swap to a low order polynomial fit over medfilt.
"""
import numpy as np
import scipy.signal as spys
import scipy.interpolate as spyi
import h5py
import os

from shutil import copy2
from scintillation_module.tools import defaultDicts

import matplotlib.pyplot as plt


def initialisation(fileName = None, optionsArr = None):
	"""Head function, delegates cleaning to sub forunctions after extracting options / preparing dataset files
	
	Args:
	    fileName (string, optional): Source Dynspec Pipeline v2 outfile h5 file location
	    optionsArr (dict, optional): Dictionary from ..tools.defaultDicts defining cleaning parameters
	
	Raises:
	    RuntimeError: Error if neither parameter is provided as we don't know what we're trying to pocess.
	"""

	# Get the default cleaning dictionary if we are only given an input file name
	if not optionsArr:
		if fileName:
			optionsArr = defaultDicts.dSpecCleaningOptions(fileName)['customFlagger']
		else:
			raise RuntimeError("Insufficient parameters to process dataset.")

	# Grab the input/output file names from the dictiarionaries
	inputFile = optionsArr['inputFile']
	outputFile = optionsArr['outputFile']

	# Check if we can copy the raw file with default settings
	if optionsArr['cloneStructure']:
		if os.path.isfile(outputFile) and not optionsArr['overwriteByDefault']:
			if optionsArr['askIfExists']:
				if bool(raw_input("A file currently exists in the provided output location, overwrite it? y/[n]") == "y" or False):
					copy2(inputFile, outputFile)
				elif not bool(raw_input("Skip the copy operation? [y]/n") == 'y' or True):
					raise RuntimeError("File Exists and requested not to overwrite by manual input.")
			else:
				raise RuntimeError("File Exists and requested not to overwrite by dictionary values.")
		else:		
			copy2(inputFile, outputFile)

	# Check the channels to process, convert to a slice as h5 doesn't like arrays or Nones
	channelsIdx = optionsArr['outputChannels'] or [None, None]
	channels = slice(channelsIdx[0], channelsIdx[1])


	# Load the input/output file, start processing.
	with h5py.File(inputFile, 'r') as rawRef:
		with h5py.File(outputFile, 'a') as cleanRef:

				# Get the bandpass curve.
				normalisationCuvre, timeSamples = normaliseBandpass(rawRef, optionsArr)

				segSize = optionsArr['curvingSampleMax']
				rawWork = rawRef[optionsArr['mainBeamName'] + "/DATA"]
				segments = int(timeSamples / segSize) + 1

				# Process each beam individually
				# TODO: Multithread
				for beam in rawRef.keys():
					if "DYNSPEC" in beam:
						beamData = beam + "/DATA"
						rawWork = rawRef[beamData]

						startIdx = 0

						# Don't overshoot the data length
						endIdx = min(segSize, timeSamples)
						for idx in range(segments):
							workChunk = rawWork[startIdx:endIdx, channels, :]
							workChunk /= normalisationCuvre

							#UNTRUSTED
							#workChunk = weakRfiFlag(workChunk, optionsArr)
							
							# If we are only processing certain channels, copy them over
							if not channelsIdx[0]:
								cleanRef[beamData][startIdx:endIdx, channels, :] = workChunk[:, channels, :]
							
							# Otherwise clean the dataset and resize for efficient storage
							else:
								del cleanRef[beamData]
								shape = np.array(workChunk.shape)
								shape[1] = channelsIdx[1] - channelsIdx[0]
								cleanRef.create_dataset(beamData, tuple(shape), data = workChunk, compression = "lzf")
							
							# Increase indicies, check final edge case.
							# h5py may only return data to the end, but you can't write data beyond the end without errors.
							startIdx += segSize
							endIdx += segSize

							if idx == segments -2:
								startIdx = -1 * segSize
								endIdx = timeSamples + 1



def normaliseBandpass(rawRef, optionsArr):
	"""Given a dattaset, normalise the channel intensities through random sampling.
	
	Args:
	    rawRef (h5py.File): Loaded h5 file to process
	    optionsArr (dict): Dictionary of options for samplnig.
	
	Returns:
	    np.array: Array of normalisation values on the range [0,1) for channel normlisation
	"""

	# Generate a slice object for the channels of interest
	channels = optionsArr['outputChannels'] or [None, None]
	channels = slice(channels[0], channels[1])
	mainBeam = rawRef[optionsArr['mainBeamName'] + "/DATA"][:, channels ,:]


	timeSamples = mainBeam.shape[0]
	curvingSampleSize = int(min(optionsArr['curvingSampleFrac'] * timeSamples, optionsArr['curvingSampleMax']))
	trueFrac = float(curvingSampleSize) / float(timeSamples)
	randVals = np.random.rand(optionsArr['curvingSamples'])

	# Check for empty samples, assume no subsequent samples
	zeroVal = mainBeam[:, int(mainBeam.shape[1] / 2.), 0] == 0.
	if zeroVal.any():
		timeSamples = int(np.argwhere(zeroVal)[0][0] - 1)
		print("We detected a zero in timesample {0}, we will only sample up to this timestep.".format(timeSamples))

	# Get the randomly selected samples
	startSample = randVals * (1. - trueFrac) * timeSamples
	startSample = np.array(startSample, dtype = int)
	endSample = startSample + curvingSampleSize

	medianVals = []
	plotBool = optionsArr['debugPlot']

	# For n random starting indicies,
	for startIdx, endIdx in zip(startSample, endSample):
		processChunk = mainBeam[startIdx:endIdx, :, 0]

		medianVal = np.nanmedian(processChunk, axis = 0)

		# If we have a 0'd time sample, skip the iteration
		if (medianVal == 0.).any():
			continue

		# Determine slopes between channels
		slopes = np.gradient(medianVal)
		
		# Perform normalisation by the previous / next time sample depending on the sign of the slope
		# Assume first sample has a 0 slope
		normSlopesArr = np.zeros_like(slopes)[1:]
		normSlopesArr[slopes[1:] > 0] = (slopes[1:] / medianVal[:-1])[slopes[1:] > 0]
		normSlopesArr[slopes[1:] < 0] = (slopes[1:] / medianVal[1:])[slopes[1:] < 0]
		normSlopesArr = np.hstack([[0], normSlopesArr])

		replaceIdx = testFunc(normSlopesArr)

		if plotBool:
			plt.figure(3)
			plt.title("Median Vals")
			plt.plot(medianVal)
			plt.figure(2)
			plt.title("Normalised Slopes")
			plt.plot(normSlopesArr)
			plt.gca().twinx().plot(slopes, c = 'g')

		# Use a linear interpolation to resample the bandpass for heavily contaminated samples
		medianVal[replaceIdx] = spyi.interp1d(np.arange(medianVal.shape[0])[~replaceIdx], medianVal[~replaceIdx], 'linear', fill_value = "extrapolate")(np.arange(medianVal.shape[0])[replaceIdx])
		medianVals.append(medianVal)

		if plotBool:
			plt.figure(3)
			plt.title("Median Values (Post Interpolation Resample)")
			plt.plot(medianVal)
			plt.twinx().plot(replaceIdx, 'r')
			plt.show()


	# Extract the median values from all samples
	medianVals = np.array(medianVals)
	medianMedianVals = np.nanmedian(medianVals, axis = 0).reshape(-1, 1)

	# Normalise on the range of 0-1
	medianMedianVals /= np.nanmax(medianMedianVals)
	filtMed = spys.medfilt(medianMedianVals.reshape(-1), 7).reshape(-1, 1)

	if plotBool:
		plt.title("Processed Median Values")
		plt.plot(medianVals.T)
		plt.figure(2)
		plt.title("Devisation between Processed Median values and Chosen Samples")
		print(medianMedianVals.shape)
		plt.plot(medianVals.T - medianMedianVals)
		plt.figure(3)
		plt.title("Median Med-FIlt Values")
		plt.plot(medianMedianVals)
		plt.plot(filtMed)
		plt.show()

	return filtMed, timeSamples

def testFunc(inputVar):
	"""RFI Flagger: Return fallged indicies for a given dataset
	
	Args:
	    inputVar (np.ndrarray): Input datatset
	
	Returns:
	    np.ndarray: bool-type array of flagged indicies
	"""
	medianVal = np.nanmedian(np.abs(inputVar))
	workVal = None
	cacheArr = np.zeros_like(inputVar, dtype = bool)
	for idx, element in enumerate(inputVar):
		if workVal:
			if element < -0.8 * workVal:
				workVal = None
			else:
				cacheArr[idx: idx + 2] = True

		else:
			if element > 30. * medianVal:
				workVal = element
				cacheArr[idx -1: idx + 1] = True

	return cacheArr

def weakRfiFlag(dataSet, optionsArr):
	"""Low-Accuracy RFI Flagging System
	
	Args:
	    dataSet (np.ndarray): Input data
	    optionsArr (dict): Cleaning parameters dictionary
	
	Returns:
	    np.ndarray: Flagged / resampled dataset
	"""
	medianVals = np.median(dataSet).reshape(-1)
	percentile66 = np.percentile(dataSet, 66, axis = 0).reshape(-1)
	percentile33 = np.percentile(dataSet, 33, axis = 0).reshape(-1)

	approxStd = percentile66 - percentile33
	stdMul = optionsArr['stdThresholdMul']

	for idx, timeStep in enumerate(dataSet):
		timeStep = timeStep.reshape(-1)
		replaceIdx = np.logical_or(timeStep > (medianVals[idx] + stdMul * approxStd[idx]), timeStep < (medianVals[idx] - stdMul * approxStd[idx]) )

		if replaceIdx.any():
			filteredTimeStep = spys.medfilt(timeStep, 9)
			timeStep[replaceIdx] = filteredTimeStep[replaceIdx]
			dataSet[idx] = timeStep

	return dataSet

def lazyRFIResampler(dataset):
	"""Low-Accuracy Resampler for flagged RFI points. Advise not using, purely used as a test case and does not produce useful results.
	
	Args:
	    dataset (np.ndarray): Input flagged dataset
	
	Returns:
	    np.ndarray: Resampled dataset
	"""
	medVal = np.median(dataset)
	percentile66 = np.percentile(dataset, 66)
	percentile33 = np.percentile(dataset, 33)
	approxStd = percentile66 - percentile33

	replaceIdx = np.logical_or(dataset > (medVal + 5. * approxStd), dataset < (medVal - 5. * approxStd))

	if replaceIdx.any():
		oddTimeSeconds = int(1.5 / dataset.timeStep)
		oddTimeSeconds += oddTimeSeconds % 2 + 1
		medFilt = spys.medfilt(dataset, oddTimeSeconds)
		dataset[replaceIdx] = medFilt[replaceIdx]

	return dataset
