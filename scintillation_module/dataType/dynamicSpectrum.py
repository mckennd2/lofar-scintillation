"""Dyanmic spectrum class constructor. Used for all processes related to dynamic spectra for easy metadata access.

Attributes:
    currentVersion (float): Current version of the implementation (for distinguishing between output file formats)
"""
import numpy as np
import h5py
import os
import logging

from astropy.time import Time

from scintillatingObject import ScintillatingObject
from observationChunk import ObservationChunk

from ..scintillationCalculator import scintillationHandler
from ..tools import h5ReadTools as readTools
from ..tools import defaultDicts
from ..dataProcessor import rfiFlagger as rfif
from ..visualisation import plotHandler

global currentVersion 
currentVersion = 0.1

class DynamicSpectrum(ScintillatingObject):
	"""Head dynamic spectrum object: holding the main iteration functions and global variables that
		we will need to access while processing dynamic spectra.
	
	Attributes:
	    adjTimestep (float): Attribute modified if the time step is adjusted from resampling the dataset
	    beamList (list): List of beam datasets, following the format ['DYNSPEC_000/DATA', 'DYNSPEC_001/DATA', ...]
	    frequencies (TYPE): Channel frequencies in MHz
	    ios (bool): Flag for toggling to ionsopheric inspection mode. Unfinished.
	    mainBeam (str): Location of the main beam dataset
	    obsTime (string): Start/end of the observation
	    offsourceBeams (bool): Boolean denoting whether non-000 beams represent off source beams or on source beams from other stations
	    processingOptions (dict): Parameters used when determining the scintillation index of a time chunk
	    resampleOptions (dict)): Parameters used when resampling the dataset for cleaner analysiss
	    segmentInfo (dict): Parameters used when segmenting the dataset for analysis
	    sourceFile (string): Location of the source h5 file
	    sourceName (string): Name of the observed object.
	    sourceShape (array-like): Shape of the beam datasets prior to segmentation
	    timeStep (float): Sampling interval between discrete data points
	    visDict (dict): Dictionary for visualisaiton parameters, obtain from defaultDicts
	"""
	
	def __init__(self, source_file, segment_info = None, offsource_beams = None, resample_options = None, visualisation_options = None):
		"""Dynamic Spectrum Initialisation
		
		Args:
		    source_file (string): Location of the source h5 file
		    segment_info (list, optional): Parameters used when segmenting the dataset for analysis, automatically generated from 
		    									defaultDicts if not provided
		    offsource_beams (bool, optional): Boolean denoting whether non-000 beams represent off source beams or on source beams 
		    									from other stations, determined from station names if not defined.
		    resample_options (dict, optional): Dictionary describing the options for resamplnig the dataset during processing
		    visualisation_options (dict, optional): Dictionary for visualisaiton parameters, obtain from defaultDicts
		
		"""
		super(DynamicSpectrum, self).__init__(base_version = currentVersion, type_var = 'dynamicSpectrum')

		with h5py.File(source_file, 'r') as refFile:
			self.sourceFile = source_file
			self.sourceName = refFile.attrs['TARGET']
			self.adjTimestep = 1.

			self.beamList = readTools.beamList(refFile)

			self.sourceShape = refFile[self.beamList[0]].shape
			self.timeStep = refFile.attrs["SAMPLING_TIME"] / 1e6
			self.obsTime = [refFile.attrs["OBSERVATION_START_UTC"].split("Z")[0], refFile.attrs["OBSERVATION_END_UTC"].split("Z")[0]]

			self.processingOptions = {'method': 'None', 'None': None} # Placeholder if imporoperly called, but still allows for iteration
			self.mainBeam = "DYNSPEC_000/DATA"

			self.frequencies = refFile['DYNSPEC_000/COORDINATES/SPECTRAL'].attrs['AXIS_VALUE_WORLD'] / 1e6
			# Is this needed? Never used, method abandoned after we couldn't synthesize the background.
			self.offsourceBeams = offsource_beams or readTools.determineBeamType(refFile)

			self.segmentInfo = segment_info or defaultDicts.dSpecSegmentOptions()
			self.resampleOptions = resample_options or defaultDicts.dSpecResampleOptions()
			self.visDict = visualisation_options or defaultDicts.dSpecVisualisationOptions()
			self.ios = False # Requires manual initialisation.

			self.checkSegmentTime()
			
			refFile.close()


# It should be possible to abstract the three segment functions (here / imageCube) a bit more into ScintillatingObject.
	def segmenter(self, includeIndex = False):
		"""Summary
		
		Args:
		    includeIndex (bool, optional): Return a list of [index, data] if True. Useful for parallelised operations.
		"""
		if self.ios:
			print("IoScint flag enabled. You will need to reshape outputs as we will fill the off beam with 0's to allow for processing, making each beam take up a timestep.")
			return self.segmenterNoOffSource(includeIndex)
		else:
			return self.segmenterOffSource(includeIndex)


	def segmenterOffSource(self, includeIndex = False):
		"""Default beam segmenter for observations with on and off-source beams. Yields datachunks with dimensions (timestep, channels, 2), 
			where 2 is due to the on-source beam being stacked with an off-source beam.
		
		Yields:
		    ObservationChunk: A datachunk for analysis.
		
		Args:
		    includeIndex (bool, optional): Return a list of [index, data] if True. Useful for parallelised operations.
		"""

		# Make sure the segment length wasn't modified in the mean time.
		self.checkSegmentTime()

		# Initialise by the super
		segmentSize, segmentOverlap, trueSegmentSize = super(DynamicSpectrum, self).segmentprep()

		with h5py.File(self.sourceFile, 'r') as refFile:
			# Extract references from the h5 file
			onBeamRef = refFile[self.mainBeam]
			mainLength = onBeamRef.shape[0]
			nonOnSource = self.beamList != self.mainBeam
			offBeamRefs = [[refFile[dataName], dataName] for dataName in self.beamList[nonOnSource]]

			# Skip the first 5 seconds as we often see a growth in power over the first few samples, causing issues for the FFT/std operations
			startIdx = int(5. / self.timeStep)

			# If the dataset is short, only return a single segment with all of the data
			if trueSegmentSize > (1. - segmentOverlap * 1.5)* mainLength:
				rangeLimit = 1
				endIdx = mainLength
				print("We are analysing over {0}\% of the data in one segment; we will only provide one index for the entire observation.".format((1. - segmentOverlap  * 1.5) * 100))
				segmentOverlap = 0.
			else:
				rangeLimit = int(mainLength / segmentSize) + 1
				endIdx = int(trueSegmentSize + startIdx)

			# Determine the shape of the skeleton structure
			skeletonShape = np.concatenate([[min(trueSegmentSize, onBeamRef[startIdx:endIdx, ...].shape[0])], self.sourceShape[1:], [len(self.beamList)]])
			skeletonSegment = np.array([startIdx, endIdx, segmentOverlap]) #startIdx, endIdx, overlapFrac

			# To create structure / fill in. We can modify on the fly faster than creating new objects every time.
			workChunk = ObservationChunk(dataSet = np.zeros(skeletonShape), parent_object = self, 
							segment_info = skeletonSegment, processing_options = self.processingOptions[self.processingOptions['method']], 
							sampling_options = self.resampleOptions, chunk_description = [self.mainBeam, None]) 

			# Iterate over the entire dataset
			for idx in range(rangeLimit):
				workChunk[..., 0] = onBeamRef[startIdx:endIdx, ...]
				workChunk.segmentInfo[[0,1]] = [startIdx, endIdx]
				workChunk.chunkDescription = []

				for offIdx, offBeam in enumerate(offBeamRefs):
					workChunk[..., offIdx + 1] = offBeam[0][startIdx:endIdx, ...]
					workChunk.chunkDescription.append(offBeam[1])

				if includeIndex:
					yield[idx, workChunk.copy()]
				else:
					# We assume no parallelisation if the index is not included, so we don't have to copy the array.
					yield workChunk

				print('Yielded indicies {0}-{1}, with array shape {2}'.format(startIdx, endIdx, workChunk.shape))

				# If we are on the first iteration, reset inidicies to the begining.
				if idx == 0:
					endIdx -= startIdx
					startIdx = 0

				startIdx = (idx + 1) * segmentSize
				endIdx = startIdx + trueSegmentSize

				#Edge case: for small sample sizes, the overlap function may overstretch the data set in the second last sample. In this case, get the delta and shift a bit.
				if endIdx > mainLength:
					delta = endIdx - mainLength
					startIdx -= delta
					endIdx -= delta

				# On the last sample, include a final full dataset
				if (idx == rangeLimit - 2):
					endIdx = self.sourceShape[0] * 2 # h5py doesn't return data if we go overboard, ensure we get every sample.
					startIdx = trueSegmentSize * -1

			refFile.close()

	def segmenterNoOffSource(self, includeIndex = False):
		"""Default beam segmenter for observations with on and off-source beams. Yields datachunks with dimensions (timestep, channels, 2), 
			where 2 is due to the on-source beam being stacked with an off-source beam.
		
		Yields:
		    ObservationChunk: A datachunk for analysis.
		
		Args:
		    includeIndex (bool, optional): Return a list of [index, data] if True. Useful for parallelised operations.
		"""

		# Make sure the segment length wasn't modified in the mean time.
		self.checkSegmentTime()

		# Initialise by the super
		segmentSize, segmentOverlap, trueSegmentSize = super(DynamicSpectrum, self).segmentprep()

		with h5py.File(self.sourceFile, 'r') as refFile:
			# Extract references from the h5 file
			firstBeamRef = refFile[self.mainBeam]
			mainLength = firstBeamRef.shape[0]
			beamRefs = [[refFile[dataName], dataName] for dataName in self.beamList]
			beamCount = len(beamRefs)

			# Skip the first 5 seconds as we often see a growth in power over the first few samples, causing issues for the FFT/std operations
			startIdx = int(5. / self.timeStep)

			# If the dataset is short, only return a single segment with all of the data
			if trueSegmentSize > (1. - segmentOverlap * 1.5)* mainLength:
				rangeLimit = 1
				endIdx = mainLength
				print("We are analysing over {0}\% of the data in one segment; we will only provide one index for the entire observation.".format((1. - segmentOverlap  * 1.5) * 100))
				segmentOverlap = 0.
			else:
				rangeLimit = int(mainLength / segmentSize) + 1
				endIdx = int(trueSegmentSize + startIdx)

			skeletonShape = np.concatenate([[min(trueSegmentSize, firstBeamRef[startIdx:endIdx, ...].shape[0])], self.sourceShape[1:], [2]])
			skeletonSegment = np.array([startIdx, endIdx, segmentOverlap]) #startIdx, endIdx, overlapFrac

			# To create structure / fill in. We can modify on the fly faster than creating new objects every time.
			workChunk = ObservationChunk(dataSet = np.zeros(skeletonShape), parent_object = self, 
							segment_info = skeletonSegment, processing_options = self.processingOptions[self.processingOptions['method']], 
							sampling_options = self.resampleOptions, chunk_description = [self.mainBeam, None]) 

			# Iterate over the entire dataset
			for stepIdx in range(rangeLimit):
				print(startIdx, endIdx, mainLength)
				workChunk.segmentInfo[[0,1]] = [startIdx, endIdx]

				for beamIdx, beamRef in enumerate(beamRefs):
					print(startIdx, endIdx, mainLength, beamIdx + (stepIdx * beamCount), beamIdx, stepIdx)
					workChunk[..., 0] = beamRef[0][startIdx:endIdx, ...]
					#workChunk[..., 1] = generateOffSourceBeam() # If we ever find a way to synthesize the backgound, put it in here.
					workChunk.chunkDescription[0] = beamRef[1]

					if includeIndex:
						yield[beamIdx + stepIdx * beamCount, workChunk.copy()]
					else:
						# We assume the non-indexed calls are not paralellised, so we don't have to copy the array
						yield workChunk

				# If we are on the first iteration, reset inidicies to the begining.
				if stepIdx == 0:
					endIdx -= startIdx
					startIdx = 0
				startIdx = (stepIdx + 1) * segmentSize
				endIdx = startIdx + trueSegmentSize

				#print(idx, rangeLimit)
				#Edge case: for small sample sizes, the overlap function may overstretch the data set in the second last sample. In this case, get the delta and shift a bit.
				if endIdx > mainLength:
					delta = endIdx - mainLength
					startIdx -= delta
					endIdx -= delta

				# For the final return,, use the final full length sample.
				if (stepIdx == rangeLimit - 2):
					endIdx = self.sourceShape[0] * 2 # h5py doesn't return data if we go overboard, ensure we get every sample.
					startIdx = trueSegmentSize * -1

			refFile.close()

class RawSpectrum(DynamicSpectrum):
	"""A dynamic spectrum subclass used to mitigate RFI in observations so they are ready for processing.
	
	Attributes:
	    cleaningOptions (dict): Parameters for the RFI cleaner.
	    pastCleaning (dict): Temporary attribture to be filled after cleaning, used when generating PreparedSpectrum objects
	    sourceFile (string): Location of Dynspec v2 h5 file
	"""
	
	def __init__(self, cleaning_options = None, *args, **kwargs):
		"""Initialiser, passes most variables to DynamicSpectrum.
		
		Args:
		    cleaning_options (dict, optional): Optional cleaning options dictionary; will be generated from defaultDicts if not provided
		    *args: Pass variables to DynamicSpectrum
		    **kwargs: Pass variables to DynamicSpectrum
		"""
		super(RawSpectrum, self).__init__(*args, **kwargs)

		self.cleaningOptions = cleaning_options or defaultDicts.dSpecCleaningOptions(self.sourceFile)
		self.pastCleaning = None


	def clean(self):
		"""Perform a cleaning iteration on the provided dataset with the options saved in self.processingOptions
		
		Returns:
		    PreparedSpectrum: The corresponding cleaned spectrum object for analysis.
		"""
		rfif.initialisation(fileName = self.sourceFile, optionsArr = self.cleaningOptions['customFlagger'])

		self.cleaningOptions = self.cleaningOptions['customFlagger']
		self.sourceFile = self.cleaningOptions['outputFile']
		self.pastCleaning = self.cleaningOptions

		return self.preparedSpectrum()

	def preparedSpectrum(self):
		"""Generate the corresponding PreparedSpectrum object, assuming that default options have been used.
		
		Returns:
		    PreparedSpectrum: The corresponding cleaned spectrum object for analysis.
		
		Raises:
		    IOError: Raised if a cleaned version of the file does not exist
		"""
		if not os.path.exists(self.cleaningOptions['outputFile']):
			raise IOError("Twin does not yet exist. Have you run a cleaning method yet or provided the right location?")

		self.__class__ = PreparedSpectrum
		self = self.selfinit(past_cleaning = self.pastCleaning, processing_method = None)

		return self



class PreparedSpectrum(DynamicSpectrum):
	"""A dynamic spectrum subclass used to perform scintillation index analysis.
	
	Attributes:
	    indices (np.ndarray): When processed, the scintillation indices and the method used will be stored here
	    pastCleaning (dict): If generated from a RawSpectrum operation, the historic cleaning settings used are stored here.
	    processingOptions (dict): Options used when calculating the scintillation indices for the observation.
	"""
	
	def __init__(self, past_cleaning = None, index_method = None, *args, **kwargs):
		"""Summary
		
		Args:
		    past_cleaning (dict, optional): Used to pass cleaning options if the spectrum was generated by a RawSpectrum call.
		    index_method (dict, optional): The intended method to index the data, generate from defaultDict if not provided.
		    *args: Pass variables to DynamicSpectrum
		    **kwargs: Pass variables to DynamicSpectrum
		"""
		super(PreparedSpectrum, self).__init__(*args, **kwargs)
		
		self.pastCleaning = past_cleaning
		self.processingOptions = index_method
		self = self.selfinit(past_cleaning, index_method)


	def selfinit(self, past_cleaning = None, processing_method = None):
		"""Abstract some initialisation parameters so they can be accessed by RawSpecturm if needed
		
		Args:
		    past_cleaning (dict): If we are aware of the processing, we will add them as an attribute for reference
		    processing_method (dict): Dictionary for processing scintillation indices
		
		Returns:
		    PreparedSpectrum: self object, to finish __init__ and return object when used.
		"""
		if not past_cleaning:
			# Check if the cleaning attributes were saved in the cleaned file
			with h5py.File(self.sourceFile, 'r') as refFile:
				if refFile.attrs.__contains__('cleaningOptions'):
					self.pastCleaning = refFile.attrs['cleaningOptions']
				refFile.close()
		else:
			self.pastCleaning = past_cleaning

		self.processingOptions = processing_method or defaultDicts.dSpecIndexOptions()

		return self

	def determineIndex(self):
		"""Perform index analysis with the given dataset and options saved in self.processingOptions
		
		Returns:
		    np.ndarray: The calculated indices for the spectrum.
		"""
		indices = scintillationHandler.dynamicSpectrumHandler(self)
		self.indices = [indices, self.processingOptions]

		return indices

	def visualise(self, errorBars = True, trimData = False, medianVals = [False, False], freqIdx = None):
		"""Take the indicie in self.indices and output a set of graphs of the results
		
		Args:
		    errorBars (bool, optional): Toggle error bars
		    trimData (bool, optional): Remove first / last data points as they often contain errors
		    medianVals (list, optional): Take the median value from each set of beam samples
		    freqIdx (None, optional): Chose the frequency to output results from
		
		Raises:
		    RuntimeError: Trying to visualise the data before it has been generated.
		"""
		if len(self.indices) == 0:
			raise RuntimeError("The indicies have not been calculated yet.")
		resampleSteps = self.resampleOptions['resampleSteps'][1]

		if freqIdx == 'all':
			for freqIdx in range(int(self.sourceShape[1] / resampleSteps)):
				self.__visFunc(freqIdx, resampleSteps, errorBars, trimData, medianVals)

		elif not freqIdx == None:
			self.__visFunc(freqIdx, resampleSteps, errorBars, trimData, medianVals)

		else:
			plotHandler.singleObjHead(self, [errorBars, trimData, medianVals])

	def __visFunc(self, freqIdx, resampleSteps, errorBars, trimData, medianVals):
		"""Interal visualisation parser
		"""
		trueFreqIdx = freqIdx * resampleSteps
		freqStart = self.frequencies[trueFreqIdx]
		freqEnd = self.frequencies[min(trueFreqIdx + resampleSteps, self.sourceShape[1] - 1)]
		freqIdx = [freqIdx, freqStart, freqEnd]

		plotHandler.singleObjHead(self, [errorBars, trimData, medianVals], freqIdx)


class SpectralSeries(DynamicSpectrum):

	"""An object designed to handle several observations of the same source.
	
	Attributes:
	    adjTimestep (float): Description
	    beamList (TYPE): Description
	    beamLists (TYPE): Description
	    forceNewXVals (bool): Description
	    frequencies (TYPE): Description
	    indices (TYPE): Description
	    ios (bool): Description
	    mainBeam (str): Description
	    obsStartTime (TYPE): Description
	    obsTime (TYPE): Description
	    obsTimes (TYPE): Description
	    offsourceBeams (bool): Description
	    pastCleaning (TYPE): Description
	    processingOptions (dict): Description
	    resampleOptions (TYPE): Description
	    segmentInfo (TYPE): Description
	    sourceFile (TYPE): Description
	    sourceFiles (TYPE): Description
	    sourceName (TYPE): Description
	    sourceShape (TYPE): Description
	    sourceShapes (TYPE): Description
	    targets (TYPE): Description
	    timeStep (TYPE): Description
	    timeSteps (TYPE): Description
	    visDict (TYPE): Description
	    xVals (list): Description
	    xValsSteps (list): Description
	"""
	
	def __init__(self, source_files, segment_info = None, resample_options = None, processing_options = None, visualisation_options = None):
		"""Summary
		
		Args:
		    source_files (TYPE): Description
		    segment_info (None, optional): Description
		    resample_options (None, optional): Description
		    processing_options (None, optional): Description
		    visualisation_options (None, optional): Description
		"""
		super(DynamicSpectrum, self).__init__(base_version = currentVersion, type_var = 'dynamicSpectrumSeries')

		# Stencil in values from Dynamic Spectrum for later use on a per-observation level
		self.sourceFile = None
		self.mainBeam = "DYNSPEC_000/DATA"
		self.adjTimestep = 1.
		self.offsourceBeams = True
		self.segmentInfo = segment_info or defaultDicts.dSpecSegmentOptions()
		self.resampleOptions = resample_options or defaultDicts.dSpecResampleOptions()
		self.visDict = visualisation_options or defaultDicts.dSpecVisualisationOptions()
		self.processingOptions = processing_options or defaultDicts.dSpecIndexOptions()
		self.pastCleaning = None
		self.sourceShape = None
		self.beamList = None
		self.indices = None
		self.ios = False

		timeStepArr = []
		obsTimeArr = []
		targetArr = []
		sourceShapeArr = []
		beamListArr = []
		for fileLoc in source_files:
			retArr = self.__extractMetadata(fileLoc)
			timeStepArr.append(retArr[0])
			obsTimeArr.append(retArr[1])
			targetArr.append(retArr[2])
			sourceShapeArr.append(retArr[3])
			beamListArr.append(retArr[4])
			frequencies = retArr[5]

		self.timeSteps = timeStepArr
		self.targets = targetArr
		self.obsTimes = obsTimeArr
		self.beamLists = beamListArr
		self.frequencies = frequencies #Assume the last value
		timeVals, initTimes = self.__analyseMetadata(timeStepArr, targetArr, obsTimeArr, beamListArr)

		self.sourceName = targetArr[0]
		self.timeStep = timeStepArr[0]

		sortedTimes = [[startTime, endTime] for startTime, endTime in sorted(timeVals, key = lambda val: val[0])]
		sortedSources = [source_file for __, source_file in sorted(zip(initTimes, source_files), key = lambda val: val[0])]
		sortedBeamLists = [beamList for __, beamList in sorted(zip(initTimes, beamListArr), key = lambda val: val[0])]
		sortedSourceShapes = [sourceShape for __, sourceShape in sorted(zip(initTimes, sourceShapeArr), key = lambda val: val[0])]

		self.obsTimes = sortedTimes
		self.obsStartTime = sortedTimes[0][0]
		self.obsTime = [self.obsTimes[0][0], self.obsTimes[-1][1]]
		self.sourceFiles = sortedSources
		self.beamLists = sortedBeamLists
		self.sourceShapes = sortedSourceShapes

		self.xVals = []
		self.xValsSteps = []
		self.forceNewXVals = True


	def __extractMetadata(self, fileLoc):
		"""Extract useful metadata from a given h5py observation file
		
		Args:
		    fileLoc (string): File location to extract data from
		
		Returns:
		    list: Several datapoints regarding the observation
		"""
		with h5py.File(fileLoc, 'r') as refFile:
			timeStep = refFile.attrs["SAMPLING_TIME"] / 1e6
			target = refFile.attrs['TARGET']
			sourceShape = refFile[self.mainBeam].shape
			beamList = readTools.beamList(refFile)
			frequencies = refFile["DYNSPEC_000/COORDINATES/SPECTRAL"].attrs['AXIS_VALUE_WORLD'] / 1e6

			obsTime = [refFile.attrs["OBSERVATION_START_UTC"].split("Z")[0], refFile.attrs["OBSERVATION_END_UTC"].split("Z")[0]]

		return [timeStep, obsTime, target, sourceShape, beamList, frequencies]

	def __analyseMetadata(self, timeStepArr = None, targetArr = None, obsTimeArr = None, beamListArr = None):
		"""Ensures that all observations conform to the same standard observing parameters and if so, extract information regarding the timing of each observation
		
		Args:
		    timeStepArr (list): List of the timesteps each observation is sampled at
		    targetArr (list): List of targets for each observation
		    obsTimeArr (list): The observation times of each observation (start / end)
		    beamListArr (list, optional): The number of beams in each observation
		
		Returns:
		    np.array: Tuple-like list of lists of start/end time for each observation
			list: Start times in unix standard form
		"""
		#Ensure a common time step for all observations
		timeStepArr = np.array(timeStepArr or self.timeSteps)
		assert((timeStepArr[0] == timeStepArr).all())

		#Ensure a common target for all obsevrations
		targetArr = np.array(targetArr or self.targets)
		assert((targetArr[0] == targetArr).all())

		beamListArr = np.array(beamListArr or self.beamLists, dtype = object)
		beamListSizes = np.array([len(listVar) for listVar in beamListArr])
		#Ensure the same number of beams between observations
		assert((beamListArr[0].shape[0] == beamListSizes).all())

		timesArr = []
		initTimes = []
		for obsTime in obsTimeArr or self.obsTimes:
			startTime = Time(obsTime[0])
			endTime = Time(obsTime[1])
			timesArr.append([startTime, endTime])
			initTimes.append(startTime.unix)

		return np.vstack([timesArr]), initTimes

	def seriesSegmenter(self, callFunction, *args, **kwargs):
		"""Iterate over each observation and call a given function, with an arbitrary number of arguments

		This function will modify your given object to temporarily have the attributes of the equivilent ProcessedSpectrum, allowing
			for easy processing of datasets. If a function can be called on a standard ProcessedSpectrum, this will be able to handle it.
		
		Args:
		    callFunction (TYPE): Function to call
		    *args: Arbitrary arguments to pass
		    **kwargs: Arbitrary keyword arguments to pass
		
		Returns:
		    list: List of returned values for each observation
		"""
		returnValues = []

		getXVals = (self.xVals == []) or (self.forceNewXVals)

		if getXVals:
			self.xVals = []
			self.xValsSteps = []
		totalFiles = len(self.sourceFiles)

		multiProcCache = self.processingOptions['multiprocess']


		for idxFile, fileName in enumerate(self.sourceFiles):
			print("Begining to analyse file {0} of {1}".format(idxFile, totalFiles))
			# Init for super segmenter
			self.sourceFile = fileName
			self.beamList = self.beamLists[idxFile]
			self.obsTime = self.obsTimes[idxFile]

			self.sourceShape = self.sourceShapes[idxFile]
			self.checkSegmentTime()

			segmentSize, segmentOverlap, trueSegmentSize = super(DynamicSpectrum, self).segmentprep()
			mainLength = self.sourceShape[0]

			if trueSegmentSize > (1. - segmentOverlap * 1.5)* mainLength:
				print("We expect to only have a single segment output; we are disabling multiprocessing for this file as a result.")
				self.processingOptions['multiprocess'] = False

			print("Processing file {0} of shape {1}".format(self.sourceFile, self.sourceShape, self.beamList))

			# Iterate over the current file
			returnValues.append(callFunction(*args, **kwargs))

			self.processingOptions['multiprocess'] = multiProcCache # Revert any possible changes to multiprocessing

			if getXVals:
				# Time information for plotter
				startTime, endTime = self.obsTimes[idxFile]
				deltaTrueStart = (startTime - self.obsStartTime).value * 24
				deltaTTotal = (endTime - startTime).value * 24

				# Store the expected xValues for the current iteration
				steps = len(returnValues[-1])
				deltaT = deltaTTotal / steps
				xVal = np.hstack([np.arange(deltaT / 2., deltaTTotal - deltaT, deltaT), [deltaTTotal - deltaT / 2.]])[:steps] #Edge case: 2 values for single step indices, get the amount of values that correspond to our steps.
				self.xVals.append(deltaTrueStart + xVal)


		if self.forceNewXVals:
			self.forceNewXVals = False

		self.sourceFile = None
		self.sourceShape = np.concatenate([[None], self.sourceShape[1:]])
		self.beamList = None
		self.obsTime = [self.obsTimes[0][0], self.obsTimes[-1][1]]

		return returnValues

	def determineIndex(self):
		"""Perform index analysis with the given dataset and options saved in self.processingOptions
		
		Returns:
		    np.ndarray: The calculated indices for the spectrum.
		"""
		indices = self.seriesSegmenter(scintillationHandler.dynamicSpectrumHandler, self)
		self.indices = [indices, self.processingOptions, self.xVals]

		return indices

	def visualise(self, errorBars = True, trimData = False, medianVals = [False, False], freqIdx = None):
		"""Take the indicie in self.indices and output a set of graphs of the results
		
		Args:
		    errorBars (bool, optional): Toggle error bars
		    trimData (bool, optional): Remove first / last data points as they often contain errors
		    medianVals (list, optional): Take the median value from each set of beam samples
		    freqIdx (None, optional): Chose the frequency to output results from
		
		Raises:
		    RuntimeError: Trying to visualise the data before it has been generated.
		"""
		if len(self.indices) == 0:
			raise RuntimeError("The indicies have not been calculated yet.")
		
		resampleSteps = self.resampleOptions['resampleSteps'][1]
		if freqIdx == 'all':
			for freqIdx in range(int(self.sourceShape[1] / resampleSteps)):
				self.__visFunc(freqIdx, resampleSteps, errorBars, trimData, medianVals)

		elif not freqIdx == None:
			self.__visFunc(freqIdx, resampleSteps, errorBars, trimData, medianVals)

		else:
			plotHandler.seriesObjHead(self, [errorBars, trimData, medianVals])

	def __visFunc(self, freqIdx, resampleSteps, errorBars, trimData, medianVals):
		"""Internal visualisation function
		"""
		trueFreqIdx = freqIdx * resampleSteps
		freqStart = self.frequencies[trueFreqIdx]
		freqEnd = self.frequencies[min(trueFreqIdx + resampleSteps, self.sourceShape[1] - 1)]
		freqIdx = [freqIdx, freqStart, freqEnd]

		plotHandler.seriesObjHead(self, [errorBars, trimData, medianVals], freqIdx)