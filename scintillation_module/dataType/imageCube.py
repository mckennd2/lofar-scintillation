"""Image Cube class constructor. Used for all processes related to image cubes for easy metadata access.

Attributes:
    currentVersion (float): Current version of the implementation (for distinguishing between output file formats)
"""
import numpy as np
import h5py

from scintillatingObject import ScintillatingObject
from observationChunk import ObservationChunk

from ..scintillationCalculator import scintillationHandler
from ..tools import defaultDicts
from ..visualisation import plotHandler as pltTools

import warnings

global currentVersion 
currentVersion = 0.1

class ImageCube(ScintillatingObject):
	"""Head image cube object: holding the main iteration functions and global variables that
			we will need to access while processing the image cube.
	
	Attributes:
	    adjTimestep (float): Attribute modified if the time step is adjusted from resampling the dataset
	    analyseChannel (int): Polarisation to analyse (by axis)
	    frequencies (list): Sampled frequencies in MHz
	    groupName (string): Name of observation database in the h5 data set
	    obsStartTime (string): Initial sample time
	    obsTime (string): Start / end of the observation
	    pixels (np.ndarray-like): Identified scintillating pixels
	    processingOptions (dict): Parameters used when determining the scintillation index of a time chunk
	    resampleOptions (dict)): Parameters used when resampling the dataset for cleaner analysiss
	    segmentInfo (dict): Parameters used when segmenting the dataset for analysis
	    sourceFile (string): Observation h5 file location
	    sourceName (string): Name of the target
	    sourceShape (array-like, np.ndarray): Shape of the observation dataset
	    timeStep (float): Time interval between samples
	    visDict (dict): Dictionary for visualisaiton parameters, obtain from defaultDicts
	"""
	
	def __init__(self, source_file, group_name = "", segment_info = None, resample_options = None, channel = 0, visualisation_options = None):
		"""Initialisation
		
		Args:
		    source_file (string): Location of the source h5 file
		    group_name (str, optional): Group name of the observation within the h5 file, eg "observations/20XX-XX-XXTXX:XX:XX"
		    segment_info (list, optional): Parameters used when segmenting the dataset for analysis
		    resample_options (dict, optional): Parameters used when resampling the dataset for cleaner analysiss
		    channel (int, optional): Polarisation to analyse (by axis)
		    merge_sources (bool, optional): Whether or not to treat scintillating pixels unqieuly or merge together and perform prop. of error
		    visualisation_options (dict, optional): Dictionary for visualisaiton parameters, obtain from defaultDicts
		"""
		super(ImageCube, self).__init__(base_version = currentVersion, type_var = 'imageCube')

		with h5py.File(source_file, 'r') as refFile:
			self.sourceFile = source_file
			self.analyseChannel = channel
			self.pixels = None
			self.adjTimestep = 1.
			self.frequencies = [0., 0.] #pdate imaging automation to extract subband frequencies in future.

			if group_name:
				self.groupName = group_name
			else:
				print("Observation name not provided; defaulting to first key under {0}['observations/']".format(source_file))

				self.groupName = "observations/" + str(refFile['observations'].keys()[0])

			self.sourceName = refFile[self.groupName].attrs['target']
			self.pixels = []
			self.sourceShape = refFile[self.groupName + "/image_data"].shape
			self.timeStep = refFile[self.groupName].attrs['timeStep']
			self.obsTime = [refFile[self.groupName].attrs['init_time'], refFile[self.groupName].attrs['end_time']]
			self.obsStartTime = self.obsTime[0]
			self.processingOptions = {'method': 'None', 'None': None}

			self.segmentInfo = segment_info or defaultDicts.imagingSegmentOptions()
			self.resampleOptions = resample_options or defaultDicts.imagingResampleOptions()
			self.visDict = visualisation_options or defaultDicts.imagingVisualisationOptions()
				
			self.checkSegmentTime()

			refFile.close()

	def segmenter(self, force_all = False):
		"""Head segmentation function: pass on to segment by pre-determined pixel or pass raw image cubes.
		
		Args:
		    force_all (bool, optional): Force the passing of image cubes rather than pixel time series
		"""
		__ = self.checkSegmentTime() # Variable needs to be assigned or we will return None as the iter function

		if np.any(self.pixels) and not force_all:
			return self.__segmenter_pixels()

		return self.__segmenter_raw()

# It should be possible to abstract the three segment (two dynamicSpectrum / this one) functions a bit more into ScintillatingObject.
	def __segmenter_pixels(self):
		"""Segment by pre-identified pixels stored in self.pixels, return a timeseries for that pixel
		
		Yields:
		    ObservationChunk: Time series of a given pixel over a segment described by self.segmentInfo
		"""

		# Initialise by the super
		segmentSize, segmentOverlap, trueSegmentSize = super(ImageCube, self).segmentprep()
		skeletonShape = [trueSegmentSize]

		with h5py.File(self.sourceFile, 'r') as refFile:
			
			imageDataRef = refFile[self.groupName + "/image_data"]

			# While we note some lower pwer in samples like in the DynamicSpectrum case, a constant length for the effect could not be determined.
			# As a result, the first sample should not be trusted
			startIdx = 0
			endIdx = trueSegmentSize
			rangeLimit = int(self.sourceShape[1] / segmentSize)
			print(self.sourceShape, segmentSize, rangeLimit)
			skeletonSegment = np.array([startIdx, endIdx, segmentOverlap]) #startIdx, endIdx, overlapFrac

			# To create structure / fill in. We can modify on the fly faster than creating new objects every time.
			workChunk = ObservationChunk(dataSet = np.zeros(skeletonShape), parent_object = self, 
											segment_info = skeletonSegment, processing_options = self.processingOptions[self.processingOptions['method']], 
											sampling_options = self.resampleOptions, chunk_description = ['mergeSourcesAbandoned', None]) 

			pixelCount = self.pixels.shape[0]
			iterPixels = np.arange(pixelCount)
			for idx in range(rangeLimit):
				workChunk.segmentInfo[[0,1]] = [startIdx, endIdx]

				# Return the workChunk structure for every pixel of interest
				for pixelIdx in iterPixels:
					pixelIdx = self.pixels[pixelIdx]
					workChunk.chunkDescription[1] = pixelIdx

					workChunk[...] = imageDataRef[self.analyseChannel, startIdx:endIdx, pixelIdx[0], pixelIdx[1]]

					yield workChunk

				startIdx += segmentSize
				endIdx += segmentSize

				# Return a full sample length for the last segment
				if idx == rangeLimit -2:
					endIdx = self.sourceShape[1] * 2 # h5py doesn't return data if we go overboard, ensure we get every sample.
					startIdx = trueSegmentSize * -1

			refFile.close()

	def __segmenter_raw(self):
		"""Segment by time step, return a full image cube
		
		Yields:
		    ObservationChunk: Image Cube for a given observation over a segment described by self.segmentInfo
		"""

		# Initialise by the super
		segmentSize, segmentOverlap, trueSegmentSize = self.segmentprep()
		skeletonShape = np.concatenate([[trueSegmentSize], self.sourceShape[2:]])
		
		with h5py.File(self.sourceFile, 'r') as refFile:
			
			imageDataRef = refFile[self.groupName + "/image_data"]

			# While we note some lower pwer in samples like in the DynamicSpectrum case, a constant length for the effect could not be determined.
			# As a result, the first sample should not be trusted
			startIdx = 0
			endIdx = trueSegmentSize
			rangeLimit = int(self.sourceShape[1] / segmentSize)

			skeletonSegment = np.array([startIdx, endIdx, segmentOverlap]) #startIdx, endIdx, overlapFrac

			# To create structure / fill in. We can modify on the fly faster than creating new objects every time.
			workChunk = ObservationChunk(dataSet = np.zeros(skeletonShape), parent_object = self, 
											segment_info = skeletonSegment, processing_options = self.processingOptions[self.processingOptions['method']], 
											sampling_options = self.resampleOptions, chunk_description = [False, None]) 

			workChunk.chunkDescription[1] = np.argwhere(workChunk[0])

			# Iterate over the entire dataset
			for idx in range(rangeLimit):
				workChunk.segmentInfo[[0,1]] = [startIdx, endIdx]

				workChunk[...] = imageDataRef[self.analyseChannel, startIdx:endIdx, ...]
				yield workChunk

				startIdx += segmentSize
				endIdx += segmentSize

				# Return a full sample for the last segment
				if idx == rangeLimit - 2:
					endIdx = self.sourceShape[1] * 2 # h5py doesn't return data if we go overboard, ensure we get every sample.
					startIdx = trueSegmentSize * -1

			refFile.close()

class RawImageCube(ImageCube):
	"""A ImageCube subclass used to identify that pixels have not been identified yet -- this is needed for the power spectrum method analysis, but can be bypasses if needed.
	
	Attributes:
	    pixels (list): List of flagged pixel indices
	    scintOptions (dict): Dictionary of options used to identifying scintillating pixels
	"""
	
	def __init__(self, scintillating_options = None, *args, **kwargs):
		"""Initialisation
		
		Args:
		    scintillating_options (dict, optional): Dictionary of options used to identifying scintillating pixels
		    *args: Pass variables to ImageCube
		    **kwargs: Pass variables to ImageCube
		"""
		super(RawImageCube, self).__init__(*args, **kwargs)

		self.scintOptions = scintillating_options or defaultDicts.imagingScintillatingOptions()


	### TODO: Reimplemnt random sampling method. Lost during merge.
	def identifyPixels(self):
		"""Identify pixels by a simplistic manner, merge soruces if needed
		
		Returns:
		    PreapredCube: ImageCube subclass ready for analysis
		"""
		timeSamples = self.sourceShape[1]
		sampleLen = min(self.scintOptions['sampleFrac'] * timeSamples, self.scintOptions['sampleMaxLen'])
		
		startSample = int(timeSamples * self.scintOptions['sampleLocationFrac']) 
		endSample = int(startSample + sampleLen)

		with h5py.File(self.sourceFile, 'r') as refFile:
			pixelSample = refFile[self.groupName + "/image_data"][self.analyseChannel, startSample:endSample, ...]
			pixelMapMax = np.max(pixelSample, axis = 0)
			trueMax = np.max(pixelMapMax)
			
			pixels = np.argwhere(pixelMapMax > trueMax * self.scintOptions['fracOfMax'])
			refFile.close()

		self.pixels = pixels


		return self.preparedCube()


	def preparedCube(self, force_raw = False):
		"""Returns the complementary PreparedImageCube to this object.
		
		Args:
		    force_raw (bool, optional): Create the object even if we have not processed pixels. Will generate a list of all coordinates within the array
		
		Returns:
		    PreapredImageCube: ImageCube subclass ready for analysis
		
		Raises:
		    RuntimeError: Error if we aren't forcing raw creation and have not identified any pixels.
		"""

		if force_raw:
			if not self.pixels:
				self.pixels = np.argwhere(np.ones(self.sourceShape[2:]))
			self.__class__ = PreparedImageCube
			self = self.selfinit(index_method = None, past_processing = None)

			return self

		elif np.any(self.pixels):
			self.__class__ = PreparedImageCube
			self  = self.selfinit(index_method = None, past_processing = self.processingOptions)

			return self

		else:
			raise RuntimeError("This RawImageCube has not had any scintillating pixels identified.")

class PreparedImageCube(ImageCube):
	"""An ImageCube subclass used to perform index analysis,
	
	Attributes:
	    indices (np.ndarray): When processed, the scintillation indices and the method used will be stored here
	    pastProcessing (dict):  If generated from a RawImageCube operation, the historic cleaning settings used are stored here.
	    processingOptions (dict): Options used when calculating the scintillation indices for the observation.
	"""
	
	def __init__(self, past_processing = None, index_method = None,  *args, **kwargs):
		"""Initialisation
		
		Args:
		    past_processing (dict, optional): Contains the options used to previous identify scintillating pixels, if available.
		    index_method (dict, optional): The intended method to index the data, generate from defaultDict if not provided.
		    *args: Pass variables to ImageCube
		    **kwargs: Pass variables to ImageCube
		"""
		super(PreparedImageCube, self).__init__(*args, **kwargs)
		
		self.pastProcessing = None
		self.processingOptions = None
		self = self.selfinit(index_method, past_processing)
		

	def selfinit(self, index_method, past_processing):
		"""Further initialisation
		
		Args:
		    index_method (dict): The intended method to index the data, generate from defaultDict if not provided.
		    past_processing (dict): Contains the options used to previous identify scintillating pixels, if available.
		
		Returns:
		    self: Return self object
		"""
		self.pastProcessing = past_processing
		self.processingOptions = index_method or defaultDicts.imagingIndexOptions()


		return self

	def determineIndex(self):
		"""Perform index analysis with the given dataset and options saved in self.processingOptions
		
		Returns:
		    np.ndarray: The calculated indices for the imaging data.
		"""
		indices = scintillationHandler.imageCubeHandler(self)
		self.indices = [indices, self.processingOptions]

		return indices

	def visualise(self, errorBars = True, trimData = False, medianVals = [False, False], freqIdx = None):
		"""Take the indicie in self.indices and output a set of graphs of the results
		
		Args:
		    errorBars (bool, optional): Toggle error bars
		    trimData (bool, optional): Remove first / last data points as they often contain errors
		    medianVals (list, optional): Take the median value from each set of beam samples
		    freqIdx (None, optional): Chose the frequency to output results from
		
		Raises:
		    RuntimeError: Trying to visualise the data before it has been generated.
		"""
		if not self.indices:
			raise RuntimeError("The indicies have not been calculated yet.")

		if freqIdx is not None:
			warnings.warn("Frequencies not yet extracted from Measurement Sets/Imaging FITS", RuntimeWarning)

		pltTools.singleObjHead(self, [errorBars, trimData, medianVals])
