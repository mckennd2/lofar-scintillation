"""Skeleton to be used to match up the dynamic spectrum and imaging class functions.
"""
import copy
from ..visualisation import plotHandler as pltTools

class ScintillatingObject(object):
	"""Skeleton with minimal function outside of being a helpful mirror.
	
	Attributes:
	    baseVersion (float): Version of the object implementation
	    indices (np.ndarray, list-like): Resulting indices from calculations
	    segmentInfo (dict): Parameters for semgentation of the dataset
	    timeStep (float): Time interval between samples
	    typeVar (string): The type of object being inspected. ('dynamicSpectrum' or 'imageCube')
	"""
	
	def __init__(self, base_version, type_var):
		"""A very empty initialisation function. It will be further built on by other classes, 
			functions here to help catch errors in future.
		
		Args:
		    base_version (float): Object version
		    type_var (string): Object type (DS, IC, etc.)
		
		"""
		self.typeVar = type_var
		self.baseVersion = base_version
		self.indices = None
		self.segmentInfo = None
		self.timeStep = None


	def determineIndex(self):
		"""Index skeleton
		
		Raises:
		    NotImplementedError: Description
		"""
		raise NotImplementedError

	def segmenter(self):
		"""Iteration Skeleton
		
		Raises:
		    NotImplementedError: Description
		"""
		raise NotImplementedError

	def segmentprep(self):
		"""Head segmentation parameter helper, may be overwritten by subclasses for edge cases.
		
		Returns:
		    list: Segmentation parameters
		"""
		segmentSize = self.segmentInfo['segmentSize']
		segmentOverlap = self.segmentInfo['segmentOverlapFraction']
		print(self.typeVar)
		if 'dynamicSpectrum' in self.typeVar:
			timeIdx = 0
			print("DynSpec Sample")
		elif self.typeVar == 'imageCube':
			print("Img Sample")
			timeIdx = 1
		else:
			print("Unknown sample, default to 0.")
			timeIdx = 0
		print(int((1. + segmentOverlap) * segmentSize), self.sourceShape[timeIdx], timeIdx)
		return segmentSize, segmentOverlap, min(int((1. + segmentOverlap) * segmentSize), self.sourceShape[timeIdx])

	def checkSegmentTime(self):
		"""Verify the segmentSize is still appropriate for the timeStep by recalculating it
		"""
		print(self.typeVar)
		if 'dynamicSpectrum' in self.typeVar:
			timeIdx = 0
			print("DynSpec Sample")
		elif self.typeVar == 'imageCube':
			print("Img Sample")
			timeIdx = 1
		else:
			print("Unknown sample, default to 0.")
			timeIdx = 0
		if self.segmentInfo['segmentByTime']:
			self.segmentInfo['segmentSize'] = min(int(self.segmentInfo['segmentByTimeSeconds'] / self.timeStep), self.sourceShape[timeIdx])

	def visualise(self, *args, **kwargs):
		"""Visualisation Skeleton
		
		Args:
		    *args: Description
		    **kwargs: Description
		
		Raises:
		    NotImplementedError: Description
		"""
		raise NotImplementedError("The object has not been converted to a processed object.")

class ObservationTwins(object):

	"""Object holding a PreparedSpectrum and PreparedImageCube, with handy functions for mass index calculation and visualisation
	
	Attributes:
	    beamformed (PreparedSpectrum): PreparedSpectrum object
	    image (PreparedImageCube): PreparedImageCube object
	    resampled (str): Description
	    results (dict): Dictionary of output indices
	    typeVar (str): Description
	"""
	
	def __init__(self, processedSpectrum, processedImageCube):
		"""Object initialisation
		
		Args:
		    processedSpectrum (PreparedSpectrum): PreparedSpectrum object
		    processedImageCube (PreparedImageCube): PreparedImageCube object
		"""
		assert('PreparedSpectrum' in type(processedSpectrum).__name__)
		assert('PreparedImageCube' in type(processedImageCube).__name__)

		self.typeVar = 'ObservationTwin'
		self.beamformed = processedSpectrum
		self.image = processedImageCube
		self.resampled = None

		self.results = {
						'imaging': {'powerSpectrum': None, 'variability': None},
						'beamformed': {'powerSpectrum': None, 'variability': None},
						'raw': {'powerSpectrum': None, 'variability': None}
						}

		self.ensureCommonTimeStep()


	def ensureCommonTimeStep(self):
		"""Changes resampling options to match the timestep between the dynamic spectrum / image cube.
		
		Returns:
		    None
		"""
		print("Warning: This function assumes that a default resample dictionary is in use, otherwise there will be issues with any outputs from this object.")

		if self.beamformed.timeStep != self.image.timeStep:

			if self.image.timeStep > self.beamformed.timeStep:
				trigName = "beamformed"
				resampleSteps = int(round(self.image.timeStep / self.beamformed.timeStep))
				self.beamformed.resampleOptions['resampleSteps'][0] = resampleSteps
				self.beamformed.resampleOptions['resamplePercentile'][0] = 20
				self.beamformed.twinTimestep = resampleSteps
				self.resampled = 'beamformed'
				if resampleSteps > 5:
					self.beamformed.resampleOptions['resamplePercentile'][0] = 'sum'
			else:
				trigName = "imaging"
				resampleSteps = int(round(self.beamformed.timeStep / self.image.timeStep))
				self.image.resampleOptions['resampleSteps'] = [resampleSteps]
				self.image.resampleOptions['resampleAxis'] = [0]
				self.image.twinTimestep = resampleSteps
				self.resampled = 'image'
				if resampleSteps > 5:
					self.image.resampleOptions['resamplePercentile'] = ['sum']
				else:
					self.image.resampleOptions['resamplePercentile'] = [20]
				self.image.resampleOptions['resampleSqueeze'] = [False]

			print("We will now resample the {0} dataset every {1} data points.".format(trigName, resampleSteps))
			return
		else:
			print("Common timestep detected, no resampling needed.")
			return

	def determineIndex(self):
		"""Determine the index of the obsevrations via both the power and variability method.
		
		Returns:
		    dict: Results disctionary.
		"""
		if self.resampled == 'beamformed':
			resampledObj = copy.deepcopy(self.beamformed)
			cacheResample = [resampledObj.resampleOptions['resampleSteps'][0], resampledObj.resampleOptions['resamplePercentile'][0]]
			self.results['raw']['dataType'] = 'beamformed'
		elif self.resampled == 'image':
			resampledObj = copy.deepcopy(self.image)
			cacheResample = [None, None]
			self.results['raw']['dataType'] = 'imaging'


		for method in ["powerSpectrum", "variability"]:
			
			self.beamformed.processingOptions['method'] = method
			self.image.processingOptions['method'] = method

			self.beamformed.determineIndex()
			self.image.determineIndex()

			self.results['beamformed'][method] = copy.deepcopy(self.beamformed.indices)
			self.results['imaging'][method]  = copy.deepcopy(self.image.indices)

			if self.resampled:
				resampledObj.resampleOptions['resampleSteps'][0] = 1
				resampledObj.resampleOptions['resamplePercentile'][0] = None
				resampledObj.processingOptions['method'] = method
				resampledObj.determineIndex()
				self.results['raw'][method] = copy.deepcopy(resampledObj.indices)
				resampledObj.resampleOptions['resampleSteps'][0] = cacheResample[0]
				resampledObj.resampleOptions['resamplePercentile'][0] = cacheResample[1]


		return self.results


	def visualise(self, errorBars = True, trimData = False, medianVals = [False, False], freqIdx = None):
		"""Visualisation head
		
		Args:
		    errorBars (bool, optional): Toggle error bars
		    trimData (bool, optional): Remove first / last data points as they often contain errors
		    medianVals (list, optional): Take the median value from each set of beam samples
		    freqIdx (None, optional): Chose the frequency to output results from
		"""
		resampleSteps = self.beamformed.resampleOptions['resampleSteps'][1]
		if freqIdx == 'all':
			for freqIdxIdx in range(int(self.beamformed.sourceShape[1] / resampleSteps)):
				self.__visFunc(freqIdxIdx, resampleSteps, errorBars, trimData, medianVals)

		elif not freqIdx == None:
			self.__visFunc(freqIdx, resampleSteps, errorBars, trimData, medianVals)
			
		else:
			pltTools.twinObjHead(self, [errorBars, trimData, medianVals])

	def __visFunc(self, freqIdx, resampleSteps, errorBars, trimData, medianVals):
		"""Internal Visualisation handler
		"""
		trueFreqIdx = freqIdx * resampleSteps
		freqStart = self.beamformed.frequencies[trueFreqIdx]
		freqEnd = self.beamformed.frequencies[min(trueFreqIdx + resampleSteps, self.beamformed.sourceShape[1] - 1)]
		freqIdx = [freqIdx, freqStart, freqEnd]

		pltTools.twinObjHead(self, [errorBars, trimData, medianVals], freqIdx)
		