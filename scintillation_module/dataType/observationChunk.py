"""A time sseries / image chunk segment for analysis, consider the class as an np.ndarray with extra metadata for easier processing.
"""
import numpy as np 

class ObservationChunk(np.ndarray):

	"""ObservationChunk is a np.ndarray subclass used to pass around data durin gthe scintillation index calculation process. To avoid having to pass many variables between functions.
	
	Attributes:
	    baseVersion (float): Parent object version
	    chunkDescription (list): Description of the segment with respect to the overall dataset
	    info (TYPE): Description
	    processingOpt (TYPE): Description
	    samplingOpt (dict): Parameters used when resampling the dataset for cleaner analysis
	    segmentInfo (dict): Parameters used when segmenting the dataset for analysis
	    sourceFile (string): Location of the source h5 file
	    timeStep (float): Sampling interval between discrete data points
	    typeVar (TYPE): Description
	
	Deleted Attributes:
	    processingOptions (dict): Parameters used when determining the scintillation index of a time chunk
	"""
	
	def __new__(cls, dataSet, parent_object, segment_info, processing_options, sampling_options, chunk_description):
		"""Numpy initialisation
		
		Args:
		    dataSet (np.ndarray): Input dataset to store in the np.ndarray
		    parent_object (DynamicSpectrum / ImageCube-like): Parent pbject to extract attributes from
		    segment_info (dict): Segmentation options
		    processing_options (dict): Copy of the parent processing options.
		    sampling_options (dict): Copy of the parent sampling options
		    chunk_description (list): Description of the segment with respect to the overall dataset
		
		Returns:
		    TYPE: Description
		"""
		self = np.asarray(dataSet).view(cls)

		self.segmentInfo = segment_info
		self.processingOpt = processing_options
		self.samplingOpt = sampling_options
		self.chunkDescription = chunk_description

		self.typeVar = parent_object.typeVar
		self.timeStep = parent_object.timeStep
		self.sourceFile = parent_object.sourceFile
		self.baseVersion = parent_object.baseVersion

		return self

	def __array_finalize__(self, obj):
		"""Numpy initialisation
		
		Args:
		    obj (TYPE): ???
		
		Returns:
		    ObservationChunk: Attributes added
		"""
		if obj is None:
			return self

		self.segmentInfo = getattr(obj, 'segmentInfo', None)
		self.processingOpt = getattr(obj, 'processingOpt', None)
		self.samplingOpt = getattr(obj, 'samplingOpt', None)
		self.chunkDescription = getattr(obj, 'chunkDescription', None)

		self.typeVar = getattr(obj, 'typeVar', None)
		self.timeStep = getattr(obj, 'timeStep', None)
		self.sourceFile = getattr(obj, 'sourceFile', None)
		self.baseVersion = getattr(obj, 'baseVersion', None)

		self.__dict__.update(getattr(obj, "__dict__", {}))

	def __reduce__(self):
		"""Summary

		Returns:
		TYPE: Description
		"""
		# Get the parent's __reduce__ tuple
		pickled_state = super(ObservationChunk, self).__reduce__()
		# Create our own tuple to pass to __setstate__
		new_state = pickled_state[2] + (self.__dict__,)
		# Return a tuple that replaces the parent's __setstate__ tuple with our own
		return (pickled_state[0], pickled_state[1], new_state)

	def __setstate__(self, oldState):
		"""Summary

		Args:
		state (TYPE): Description
		"""
		self.__dict__ = oldState[-1]  # Set the info attribute
		# Call the parent's __setstate__ with the other tuple elements.

		super(ObservationChunk, self).__setstate__(oldState[0:-1])

	def applySampling(self, optionsCache = None):
		"""Function to resample the dataset as needed.
		
		Returns:
		    ObservationChunk: New object with resampled attributes and data (if any sampling is performed)
		
		Args:
		    optionsCache (None, optional): Description
		"""
		if self.samplingOpt['resampleAxis']:
			newSelf = self.copy()
			if not optionsCache:
				axisVar = self.samplingOpt['resampleAxis']
				percentileVar = self.samplingOpt['resamplePercentile']
				stepsVar = self.samplingOpt['resampleSteps']
				squeezeVar = self.samplingOpt['resampleSqueeze']
			else:
				axisVar, percentileVar, stepsVar, squeezeVar = optionsCache

			for idx, axis in enumerate(axisVar):
				steps = stepsVar[idx]
				percentile = percentileVar[idx]
				squeeze = squeezeVar[idx]
				if steps > 1:
					if axis == self.samplingOpt['timeStepAxis']:
						newSelf.timeStep *= steps

					tmpReshape = np.array(newSelf.shape)
					tmpReshape[axis] = -1
					tmpReshape = np.insert(tmpReshape, axis + 1, steps)

					#TODO: Easy cleanup / abstraction.
					modCheck = newSelf.shape[axis] % steps
					if modCheck != 0:
						selfTmp = np.delete(newSelf, newSelf.shape[axis] + np.arange(-1 * modCheck, 0), axis)
						selfTmp = np.reshape(selfTmp, tmpReshape)
					else:
						selfTmp = np.reshape(newSelf, tmpReshape)
					
					if type(percentile) == str:
						if percentile == 'mean':
							selfTmp = np.mean(selfTmp, axis = axis + 1)
						elif percentile == 'median':
							selfTmp = np.median(selfTmp, axis = axis + 1)
						elif percentile == 'sum':
							selfTmp = np.sum(selfTmp, axis = axis + 1)
						else:
							print("Unknown string; defaulting to median value.")
							selfTmp = np.median(selfTmp, axis = axis + 1)
					else:
						selfTmp = np.percentile(selfTmp, percentile, axis = axis + 1)

					newSelf.resize(selfTmp.shape)
					newSelf[...] = selfTmp[...]

				elif percentile:
					selfTmp = np.percentile(newSelf, percentile, axis = axis)

					newSelf.resize(np.insert(selfTmp.shape, axis, 1)) #Match the shape of the channel sampled data
					selfTmp.resize(np.insert(selfTmp.shape, axis, 1))
					newSelf[...] = selfTmp[...]

				if squeeze:
					newSelf = newSelf.squeeze(axis = axis)

			return newSelf
		else:
			return self
		
