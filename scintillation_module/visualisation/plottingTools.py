"""Summary
"""
import matplotlib.pyplot as plt
import numpy as np

from astropy.time import Time
import astropy.units as apyu
import os


def line(slope, intercept):
	"""Plot a line from slope and intercept
	
	Args:
	    slope (float): Slope of the line
	    intercept (float): Intercept of the line
	
	Returns:
	    plotVar (?): Matplotlib return 
	"""

	if slope != 0.:
		xVar = np.array(plt.gca().get_xlim())
		yVar = intercept + slope * xVar
		plotVar = plt.plot(xVar, yVar, '')
	else:
		plotVar = plt.axhline(y = intercept)

	return plotVar
def expline(power, constant, steps = 100):
	"""Plot a line that will be straight in log-log space with a given slope
	
	Args:
	    power (float): Exponential slope
	    constant (float): Exponential constant multiple
	    steps (int): Samples count
	
	Returns:
	    plotVar (?): Matplotlib return 
	"""
	xVar = np.linspace(start = plt.gca().get_xlim()[0], stop = plt.gca().get_xlim()[1],num = steps)
	yVar = np.power(xVar, power) * constant
	plotVar = plt.plot(xVar, yVar, '--')

	return plotVar



####


def initFigure(obsObj, frequencies = None):
	"""Initialise the figure for processing results
	
	Args:
	    obsObj (ScintillatingPbject): Head object being processed
	    frequencies (None, optional): Description
	
	Returns:
	    titleVar (?): Matplotlib return 
	    figVar (figure): Matplotlib Figure
	"""
	dpi = obsObj.visDict['dpi']
	saveRes = obsObj.visDict['saveResolution']

	figVar = plt.figure(num = 42, figsize = (saveRes[0] / dpi, saveRes[1] / dpi), dpi = dpi)

	plt.xlabel("Time of Observation (UTC)")
	plt.ylabel("Scintillation Index")

	startTime = obsObj.obsTime[0]

	if not frequencies is None:
		__, lowerMHz, upperMHz = frequencies
		titleVar = plt.title("{0} Scintillation Indices from observation starting {1} on ({2}, {3})Mhz".format(obsObj.sourceName, startTime, lowerMHz, upperMHz))
	else:
		titleVar = plt.title("{0} Scintillation Indices from observation starting {1} on ({2}, {3})Mhz".format(obsObj.sourceName, startTime, obsObj.frequencies[0], obsObj.frequencies[-1]))
	plt.grid()
	plt.subplots_adjust(left = 0.07, right = 0.93, bottom = 0.15, top = 0.9)

	return figVar, titleVar

def prepXAxis(obsObj):
	"""Get the x-axis elements for a given observation
	
	Args:
	    obsObj (ScintillatingPbject): Head object being processed
	
	Returns:
	    np.ndarray: List of time offsets from initial observation
	"""
	startTime = Time(obsObj.obsTime[0])
	endTime = Time(obsObj.obsTime[1])

	deltaTTotal = (endTime - startTime).value * 24
	steps = len(obsObj.indices[0])
	deltaT = deltaTTotal / steps

	timeArr = np.hstack([np.arange(deltaT / 2., deltaTTotal - deltaT, deltaT), [deltaTTotal - deltaT / 2.]])

	return timeArr

def adjXTicks(obsObj):
	"""Fi the x-axis labels
	
	Args:
	    obsObj (ScintillatingPbject): Head object being processed
	
	Returns:
	    (?): Matplotlib return
	"""
	startTime = Time(obsObj.obsTime[0])
	xTicks = plt.xticks()[0]
	xtTicksVar = plt.gca().set_xticklabels(labels = (startTime + xTicks * apyu.hour).iso, rotation = 45, fontdict = {'fontsize': 8}, horizontalalignment = 'right')

	return xtTicksVar



def saveShowClose(obsObj, plotBools, frequencies = None, parent = None, commentVar = ''):
	"""After completing a figure, call this function to save the data, figure, optionally show the figure and then reset the enviroment
	
	Args:
	    obsObj (ScintillatingPbject): Head object being processed
	    plotBools (list): Set of bools to determine whether or not to show [errorBars, timData, medianValues] in each figure. Used for file names.
	    frequencies (list, optional): Frequencies sampled
	    parent (ObservationTwin, optional): ObservationTwin in the case of a twin observation being processed
	    commentVar (str, optional): Extra comment to apend saved objects
	"""
	dpi = obsObj.visDict['dpi']
	saveFormat = obsObj.visDict['saveFormat']
	errorVar, trimData, medianVals = plotBools
	outputDir = obsObj.visDict['outputLoc']
	sampleLen = obsObj.segmentInfo['segmentSize']

	if frequencies:
		__, lowerMHz, upperMHz = frequencies
	else:
		lowerMHz, upperMHz = obsObj.frequencies[0], obsObj.frequencies[-1] 

	if parent is None:
		dataType = obsObj.typeVar
	else:
		dataType = parent.typeVar

	prefix = "/Obs_{0}_{1}/{2:.1f}-{3:.1f}MHz_{4}".format(obsObj.sourceName, str(obsObj.obsTime[0])[:21], lowerMHz, upperMHz, dataType)
	outputDir = outputDir + prefix
	suffix = "{0}-{1}segSize".format(obsObj.indices[1]['method'], sampleLen)

	if obsObj.visDict['saveFigure'] or obsObj.visDict['saveData']:
		if not os.path.exists(outputDir):
			 os.makedirs(outputDir)
	
		if obsObj.visDict['saveFigure']:
			figureSuffix = suffix
			if errorVar:
				figureSuffix += "-errorBars"
			if trimData:
				figureSuffix += "-trimmed"
			if medianVals[0]:
				figureSuffix += "-medianValsBeam"
			if medianVals[1]:
				figureSuffix += "-medianValsChannel"

			plt.savefig(outputDir + "/{0}-{1:23.23s}-plot-{2}{3}.png".format(obsObj.sourceName, obsObj.obsTime[0], figureSuffix, commentVar), dpi = dpi, format = saveFormat)
	
		if obsObj.visDict['saveData']:
			if parent is None:
				saveArr = obsObj.indices
			else:
				saveArr = parent.results

			np.save(outputDir + "/{0}-{1:23.23s}-{2}{3}-results.npy".format(obsObj.sourceName, obsObj.obsTime[0], suffix, commentVar), saveArr)
	
			if obsObj.typeVar == 'dynamicSpectrumSeries':
				np.save(outputDir + "/{0}-{1:23.23s}-{2}{3}-results-xvals.npy".format(obsObj.sourceName, obsObj.obsTime[0], suffix, commentVar), obsObj.indices[2])
		
	if obsObj.visDict['showFigure']:
		plt.show()

	plt.close('all')
