"""Handle plotting for our different observation / object types.
"""
import numpy as np 
import matplotlib.pyplot as plt
import astropy.units as apyu
import copy

import plottingTools as pltTools

# Nicer colours/format
plt.viridis()


#Expecting: (timestep, beam / pixel, channel (1 for img), 2 (data/err))
def singleObjHead(obsObj, plotBools, freqVar = None):
	"""Summary
	
	Args:
	    obsObj (TYPE): Description
	    plotBools (TYPE): Description
	    freqVar (None, optional): Description
	"""
	xVar = standardInit(obsObj, freqVar)

	plotVar, plotLabel = singleObjMain(obsObj, xVar, freqVar, plotBools)

	plt.legend(handles = [plotVar], labels = [plotLabel])
	standardExit(obsObj, plotBools, freqVar, commentVar = "-" + obsObj.indices[1]['method'])

def singleObjMain(obsObj, xVar, freqVar, plotBools, idxProc = None, cacheOptions = None):
	"""Summary
	
	Args:
	    obsObj (TYPE): Description
	    xVar (TYPE): Description
	    freqVar (TYPE): Description
	    plotBools (TYPE): Description
	    idxProc (None, optional): Description
	    cacheOptions (None, optional): Description
	
	Returns:
	    TYPE: Description
	"""
	indices, processingOptions = idxProc or obsObj.indices
	cacheOptions = cacheOptions or extractOptions(obsObj.visDict, processingOptions)
	
	yVar, yErr = extractData(indices, freqVar, plotBools[2])

	beamShape = yVar.shape[1]
	for beam in range(beamShape):
		plotVar = plotStandardArr(xVar, yVar[:, beam], yErr[:, beam], cacheOptions, plotBools)

	plotLabel = plotVar.get_label()
	plotLabel = plotLabel + " {0:05f}s ({1})".format(obsObj.timeStep * obsObj.adjTimestep, obsObj.adjTimestep)
	
	return plotVar, plotLabel

#Expecting: clusterfuck
def twinObjHead(parentObj, plotBools, freqVar = None):
	"""Summary
	
	Args:
	    parentObj (TYPE): Description
	    plotBools (TYPE): Description
	    freqVar (None, optional): Description
	
	Raises:
	    RuntimeError: Description
	"""
	bfX = standardInit(parentObj.beamformed, freqVar)

	imagingPower, imgPProc = parentObj.results['imaging']['powerSpectrum']
	imagingVar, imgVProc = parentObj.results['imaging']['variability']
	bfPower, bfPProc = parentObj.results['beamformed']['powerSpectrum']
	bfVar, bfVProc = parentObj.results['beamformed']['variability']

	if imagingPower.shape[0] != bfX.shape[0]:
		imgX = bfX[:imagingPower.shape[0]] # Assuming there may be less imaging data than BF data
	else:
		imgX = bfX

	imgPCache = extractOptions(parentObj.image.visDict, imgPProc, 'powerSpectrum')
	imgVCache = extractOptions(parentObj.image.visDict, imgVProc, 'variability')
	bfPCache = extractOptions(parentObj.beamformed.visDict, bfPProc, 'powerSpectrum')
	bfVCache = extractOptions(parentObj.beamformed.visDict, bfVProc, 'variability')

	
	imgPVar, imgPLabel = singleObjMain(parentObj.image, imgX, None, plotBools, [imagingPower, imgPProc], imgPCache)
	imgVVar, imgVLabel = singleObjMain(parentObj.image, imgX, None, plotBools, [imagingVar, imgVProc], imgVCache)
	bfPVar, bfPLabel = singleObjMain(parentObj.beamformed, bfX, freqVar, plotBools, [bfPower, bfPProc], bfPCache)
	bfVVar, bfVLabel = singleObjMain(parentObj.beamformed, bfX, freqVar, plotBools, [bfVar, bfVProc], bfVCache)

	plotVarArr = [imgPVar, imgVVar, bfPVar, bfVVar]
	plotLabelArr = [imgPLabel, imgVLabel, bfPLabel, bfVLabel]

	resampledName = parentObj.resampled
	if resampledName:
		rawPower, rawPProc = parentObj.results['raw']['powerSpectrum']
		rawVar, rawVProc = parentObj.results['raw']['variability']
		if resampledName == "beamformed":
			rawObj = copy.deepcopy(parentObj.beamformed)
			rawX = bfX
			rawFreq = freqVar
		elif resampledName == "imaging":
			rawObj = copy.deepcopy(parentObj.image)
			rawX = imgX
			rawFreq = None
		else:
			raise RuntimeError("Unknown resampled object {0}".format(resampledName))

		rawObj.adjTimestep = 1
		rawObj.visDict['powerSpectrum']['dataFmt'] = 'g*'
		rawObj.visDict['variability']['dataFmt'] = 'k*'
		rawObj.visDict['variability']['labelName'] = 'Raw Power'
		rawObj.visDict['powerSpectrum']['labelName'] = 'Raw Power'

		rawPCache = extractOptions(rawObj.visDict, rawPProc)

		rawObj.visDict['variability']['labelName'] = 'Raw Variability'

		rawVCache = extractOptions(rawObj.visDict, rawVProc)

		rawPVar, rawPLabel = singleObjMain(rawObj, rawX, freqVar, plotBools, [rawPower, rawPProc], rawPCache)
		rawVVar, rawVLabel = singleObjMain(rawObj, rawX, freqVar, plotBools, [rawVar, rawVProc], rawVCache)

		plotVarArr += [rawPVar, rawVVar]
		plotLabelArr += [rawPLabel, rawVLabel]

	
	plt.legend(handles = plotVarArr, labels = plotLabelArr)

	standardExit(parentObj.beamformed, plotBools, freqVar, parentObj)

#Expecting: (files(timestep, beam, channel , 2 (data/err)))
def seriesObjHead(obsObj, plotBools, freqVar = None):
	"""Summary
	
	Args:
	    obsObj (TYPE): Description
	    plotBools (TYPE): Description
	    freqVar (None, optional): Description
	"""
	__ = standardInit(obsObj, freqVar)

	processingOptions = obsObj.indices[1]
	cacheOptions = extractOptions(obsObj.visDict, processingOptions)

	for idx, indices in enumerate(obsObj.indices[0]):
		plotVar, plotLabel = singleObjMain(obsObj, obsObj.indices[2][idx], freqVar, plotBools, [indices, processingOptions], cacheOptions)

	plt.legend(handles = [plotVar], labels = [plotLabel])

	standardExit(obsObj, plotBools, freqVar, commentVar = "-" + obsObj.indices[1]['method'])

#(timestep, channel)
def plotStandardArr(xVar, yVar, yErr, cacheOptions, plotBools):
	"""Summary
	
	Args:
	    xVar (TYPE): Description
	    yVar (TYPE): Description
	    yErr (TYPE): Description
	    cacheOptions (TYPE): Description
	    plotBools (TYPE): Description
	
	Returns:
	    TYPE: Description
	"""
	errorBars, trimData, medianVals = plotBools
	fmtCode, label, alpha, size = cacheOptions

	if not errorBars:
		yErr = np.full_like(yVar, None)

	if trimData and len(xVar) > 2:
		xVar = xVar[1:-1]
		yVar = yVar[1:-1]
		yErr = yErr[1:-1]

	yVar[np.isnan(yVar)] = 0. #Prevent all-nan slices causing issues

	if medianVals[1]:
		yVarCache = yVar.copy()
		yVar = np.nanmedian(yVar, axis = 1)[:, np.newaxis]
		medianErrs = np.nanargmin(np.abs(yVarCache - yVar), axis = 1).reshape(1, -1)
		medianErrs = np.hstack([np.arange(yErr.shape[0], dtype = int).reshape(1, -1).T, medianErrs.T])

		yErr = yErr[medianErrs[:, 0], medianErrs[:, 1]][:, np.newaxis]

	yErr[yErr == 0.] = None #If an errorbar is 0, don't display it
	yVar[yVar == 0.] = np.nan #Revert change above so point isn't displayed

	print(xVar.shape, yVar.shape, yErr.shape)
	for channel in range(yVar.shape[1]):
		plotVar = plt.errorbar(xVar, yVar[:, channel], yerr = yErr[:, channel], fmt = fmtCode, label = label, alpha = alpha, markersize = size)
	return plotVar


def standardInit(obsObj, freqVar):
	"""Summary
	
	Args:
	    obsObj (TYPE): Description
	    freqVar (TYPE): Description
	
	Returns:
	    TYPE: Description
	"""
	plt.close(42)
	pltTools.initFigure(obsObj, freqVar)
	xVar = pltTools.prepXAxis(obsObj)

	return xVar

def extractData(indices, freqVar, medianVals = False):
	"""Summary
	
	Args:
	    indices (TYPE): Description
	    freqVar (TYPE): Description
	    medianVals (bool, optional): Description
	
	Returns:
	    TYPE: Description
	"""
	if freqVar is None:
		freqIdx = slice(None)
	else:
		freqIdx = [freqVar[0]] # List to keep axis

	print(freqIdx)
	yVar = indices[..., freqIdx, 0]
	yVar[np.isnan(yVar)] = 0. #Prevent all-nan slices causing issues

	yErr = indices[..., freqIdx, 1]

	if medianVals[0]:
		yVarCache = yVar.copy()
		yVar = np.nanmedian(yVar, axis = 1)[:, np.newaxis]
		medianErrs = np.nanargmin(np.abs(yVarCache - yVar), axis = 1).reshape(-1)
		yErr = yErr[:, medianErrs, :]

	yErr[yErr == 0.] = None #If an errorbar is 0, don't display it
	yVar[yVar == 0.] = np.nan #Revert change above so point isn't displayed

	return yVar, yErr

def extractOptions(visDict, processingOptions, method = None):
	"""Summary
	
	Args:
	    visDict (TYPE): Description
	    processingOptions (TYPE): Description
	    method (None, optional): Description
	
	Returns:
	    TYPE: Description
	"""
	method = method or processingOptions['method']

	label = visDict[method]['labelName']
	alpha = visDict[method]['alpha']
	size = visDict[method]['size']

	if visDict[method]['plotByBeam']:
		fmtCode = visDict[method]['beaFmt']
	else:
		fmtCode = visDict[method]['dataFmt']

	return [fmtCode, label, alpha, size]

def standardExit(obsObj, plotBools, freqVar, parentObj = None, commentVar = ''):
	"""Summary
	
	Args:
	    obsObj (TYPE): Description
	    plotBools (TYPE): Description
	    freqVar (TYPE): Description
	    parentObj (None, optional): Description
	    commentVar (str, optional): Description
	"""
	pltTools.adjXTicks(obsObj)
	pltTools.saveShowClose(obsObj, plotBools, freqVar, parentObj, commentVar)
