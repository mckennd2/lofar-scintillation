"""Pass the objects through the correct methods and extract the options from their given dicts
"""
import powerMethod
import variabilityMethod
from numpy import argwhere

def dynamicSpectrumHandler(processedSpectrum):
	"""DS Handler
	
	Returns:
	    np.ndarray-like: Output indices
	"""
	methodName = processedSpectrum.processingOptions['method']

	timeResample = processedSpectrum.resampleOptions['timeStepAxis'] in processedSpectrum.resampleOptions['resampleAxis']
	if timeResample:
		processedSpectrum.adjTimestep = processedSpectrum.resampleOptions['resampleSteps'][processedSpectrum.resampleOptions['resampleAxis'].index(processedSpectrum.resampleOptions['timeStepAxis'])]

	if methodName == "powerSpectrum":
		scintFunc = lambda obsObj: powerMethod.errorsHead(obsObj, powerMethod.dynamicSpectrumHandler)
	elif methodName == "variability":
		scintFunc = variabilityMethod.dynamicSpectrumHandler
	else:
		print("Method not found: {0}, default to power spectrum method.".format(methodName))
		scintFunc = powerMethod.dynamicSpectrumHandler
		
	indices = scintFunc(processedSpectrum)

	return indices


def imageCubeHandler(imageCube):
	"""IC Hanlder
	
	Returns:
	    list-like: Output indices
	"""
	methodName = imageCube.processingOptions['method']

	timeResample = imageCube.resampleOptions['timeStepAxis'] in imageCube.resampleOptions['resampleAxis']
	if timeResample:
		imageCube.adjTimestep = imageCube.resampleOptions['resampleSteps'][imageCube.resampleOptions['resampleAxis'].index(imageCube.resampleOptions['timeStepAxis'])]

	if methodName == "powerSpectrum":
		scintFunc = lambda obsObj: powerMethod.errorsHead(obsObj, powerMethod.imageCubeHandler)
	elif methodName == "variability":
		scintFunc = variabilityMethod.imageCubeHandler
	else:
		print("Method not found: {0}, default to power spectrum method.".format(methodName))
		scintFunc = powerMethod.imageCubeHandler

	indices = scintFunc(imageCube)

	return indices
	
