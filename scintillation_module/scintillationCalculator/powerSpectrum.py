"""Backend for implementatino of the power spectrum method.
"""

import numpy as np
import scipy.signal as spys
import scipy.optimize as spyo

import matplotlib.pyplot as plt

def powerIntegral(dataSet, startStop = None, paramsPast = None, startEndVal = [None, None]):
	"""Implementation used to determine the scintillation index by integrating over a segment of the power spectrum
	
	Args:
	    dataSet (np.ndarray): Input datachunk with associated metadata
	    startStop (list, optional): If provided, skip the sigmoid fitting process and integrate over the givien indicies (we fit to the main beam,
	    								so after the first off-source beam we can pass in the same start/end indices to speed up operations)
	    paramsPast (list, optional): Pass initialisation parameters to speed up the sigmoid fitting process
	    startEndVal (list, optional): Limits of integration, elements set to None to use the power percentage in the sigmoid fitting operation
	
	Returns:
	    indices (np.ndarray): Calcuated indices for the data chunk.
	"""
	dataSet = heavyRfiResampler(dataSet)
	freqOutput, fftOutput = spys.welch(dataSet, fs = 1. / dataSet.timeStep, window = ("kaiser", 8.6), nperseg = 512, return_onesided=True)
	if startStop:
		startIdx, endIdx = startStop

		if np.isnan([startIdx, endIdx]).any():
			return np.nan, np.nan, np.nan, paramsPast
	else:
		# We expect the f = 0 case to go to infinite, we will need to correct for that by not treating the first terms.
		freqLog = np.log10(freqOutput[1:])
		fftLog = np.log10(fftOutput[1:])
		
		# In the case we have optomisation errors or bad fits, throw out the sample and return NaN's.
		try:
			startIdx, endIdx, paramsPast = sigmoidFitSpectrum(freqLog, fftLog, paramsPast, startEndVal)
			startIdx += 1
			endIdx += 1
		except (RuntimeError, IndexError, ValueError): # If we have an error, just discard the sample. Errors include fitting, covariance calculation and other oddities
			return np.nan, np.nan, np.nan, paramsPast

	powerVal = np.trapz(fftOutput[startIdx:endIdx], freqOutput[startIdx:endIdx])
	
	return powerVal, startIdx, endIdx, paramsPast


# Temp addition for handling highly RFI contaminated data
def heavyRfiResampler(dataset):
	"""RFI contaminated data
	
	Args:
	    dataset (np.ndarray): Input dataset
	
	Returns:
	    np.ndarray: Flagged and resampled 
	"""
	medVal = np.nanmedian(dataset)
	percentile66 = np.nanpercentile(dataset, 66)
	percentile33 = np.nanpercentile(dataset, 33)
	approxStd = percentile66 - percentile33

	replaceIdx = np.logical_or(dataset > (medVal + 5. * approxStd), dataset < (medVal - 5. * approxStd))

	if replaceIdx.any():
		oddTimeSeconds = int(1.5 / dataset.timeStep)
		oddTimeSeconds += oddTimeSeconds % 2 + 1
		medFilt = spys.medfilt(dataset, oddTimeSeconds)
		dataset[replaceIdx] = medFilt[replaceIdx]

	return dataset


#### Power spectrum fitting helper functions

def sigmoidFitSpectrum(xVal, yVal, paramsPast = None, startEndVal = [None, None]):
	"""Automated process for fitting a reversed sigmoid to a given function.
	
	Args:
	    xVal (np.ndarray): Sampling x-array
	    yVal (TYPE): Description
	    paramsPast (None, optional): Description
	    startEndVal (list, optional): Description
	
	Returns:
	    startIdx (int): Suggested integration start point for power spectrum
	    endIdx (int): Suggested integration end point for power spectrum
	
	Raises:
	    ValueError: Description
	"""
	
	# Normalise the dataset on the range of ~0-1, though we may have some samples abpve 1 from the early mean sample
	earlyMax = np.mean(yVal[:5])
	lateMean = np.mean(yVal[-1 * int(len(yVal) * 0.2):])
	normData = (yVal - lateMean) / (earlyMax - lateMean) #Normalise data on the range approx. [0,1]

	#Is this just getting a percentile of a single data point? 
	normLateMean = np.percentile(normData[-1 * int(len(normData) * 0.2)], 95)
	normData = np.maximum(normData, normLateMean) # Create an artificial floor for the power fitting, this making the fitting process easier

	# If we are using sample initalised parameters, make sure they are valid to speed up the process.
	# In reality, we should discard any cases where these are not met, but the data still seems reasonable either way
	if paramsPast is not None:
		paramsPast = [max(-1., min(paramsPast[0], 20.)), max(-2., min(paramsPast[1], 2.))]
		print("Using cached paramaters for initialisation", paramsPast)
	else:
		paramsPast = [1., 1.]
	
	# Fit a sigmoid to the squashed, normalised power curve
	sigmoidParam = spyo.curve_fit(reversedSigmoid, [xVal, normLateMean], normData, maxfev = 100000, p0 = paramsPast)
	if sigmoidParam[0][0] < -10.:
		raise ValueError("Fit equivilent to normal sigmoid, not reversed (higher frequency components are higher powered than lower).")
	sigmoidUpper = 0.99

	#Chose the lower sample point by the maximum value out of: the last sampled x point on the curve, the previously defined floor and 0.03
	lateSample = reversedSigmoid([xVal[-1], normLateMean], sigmoidParam[0][0], sigmoidParam[0][1])
	sigmoidLower = max(lateSample, 0.03)

	startVal, endVal = startEndVal
	if not startVal:
		startVal = inverseReversedSigmoid(sigmoidUpper, sigmoidParam[0][0], sigmoidParam[0][1])
	
	if not endVal:
		endVal = inverseReversedSigmoid(sigmoidLower, sigmoidParam[0][0], sigmoidParam[0][1])

	curveFrequencies = np.argwhere(np.logical_and(xVal > startVal, xVal < endVal))

	if curveFrequencies.size != 0:
		startIdx = curveFrequencies[0][0]
		endIdx = curveFrequencies[-1][0]
	else:
		# TODO: replace with ValueError, do not sample bad data.
		startIdx = 0
		endIdx = np.argwhere(normData == lateMean)[0][0] + 25
		print("Error: Curve incorrectly fit, defaulted to indices {0} and {1}".format(0, endIdx))

	return startIdx, endIdx, sigmoidParam[0]



def reversedSigmoid(dataFloor, a, c):
	"""A function describing the reversed sigmoid function, f(x) = 1 / (1 + e^(a(x - c)))
	
	Args:
	    dataFloor (List[np.ndarray-like, float]): List containing the dataset and a minimum value for the floor. Useful for constrained optomisation.
	    a (float): Exponential parameter for steepness of sigmoid
	    c (float): Exponential parameter for offset of the turning point from 0.
	
	Returns:
	    (float, array-like): Return for the sampled x values
	"""
	dataset, floorVal = dataFloor

	return np.maximum(np.divide(1., 1. + np.exp(a * (dataset - c))), floorVal)

def inverseReversedSigmoid(yVal, a, c):
	"""Inverse of the sigmoid function to estimate the value of x for a given y value
	
	Args:
	    yVal (float, array-like): Output of the sigmoid (on range [0,1])
	    a (float): Exponential parameter for steepness of sigmoid
	    c (float): Exponential parameter for offset of the turning point from 0.
	
	Returns:
	    (float, array-like): Solutions to given input
	"""
	return np.divide(np.log(np.divide(1., yVal) - 1.), a) + c


def reversedSigmoidSlope(dataset, a, c):
	"""Return the float of the sigmoid at a given point f'(x) = -a * e^(a(x - c)) / (1 + ^(a(x - c)))^2
	
	Args:
	    dataset (float, array-like): Sample x locations
	    a (float): Exponential parameter for steepness of sigmoid
	    c (float): Exponential parameter for offset of the turning point from 0.
	
	Returns:
	    float, array-like: The sigmoid slope at a given location.
	"""
	exponential = np.exp(a * (dataset - c))
	return -1. * np.divide(a * exponential, np.square(exponential + 1))
